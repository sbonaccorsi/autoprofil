package com.SbDev.AutoProfil.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.Profils.ProfilsListActivity;
import com.SbDev.AutoProfil.Profils.Edit.EditProfilActivity;
import com.SbDev.AutoProfil.data.Local.DataInterface.PreferenceFields;
import com.SbDev.AutoProfil.data.Local.DataInterface.ProfilConstant;
import com.SbDev.AutoProfil.data.Local.Profil;
import com.SbDev.AutoProfil.data.Local.ProfilManagement;
import com.SbDev.AutoProfil.data.provider.AutoProfilContent.Profils;
import com.bonadevapps.compoments.preferences.PreferenceManager;

public class ManageProfilService extends Service implements LocationListener {

	private static final int						TIME_INTERVAL_CHECK_ACTIVE_PROFIL = 5000;

	private ProfilManagement						mProfilManager;

	private NotificationManager						mManagerNotification;
	private Uri										mUriNotification;

	private Handler									mHandlerProfil;

	private LocationManager							mLocationManager;
	
	private int										mRadiusResearch;
	private int										mDistMinToRefreshLocation;
	private int										mTimeMinRefreshLocation;

	// TODO
	// Maybe delete the handler and do this check in locationUpdate callback
	private Runnable								mManagmentService = new Runnable() {

		@Override
		public void run() {
			Log.e("[DEBUG ManageProfilService onStartCommand]", "Check profil enable");
			if (haveProfilsEnabled())
				mHandlerProfil.postDelayed(mManagmentService, TIME_INTERVAL_CHECK_ACTIVE_PROFIL);
			else
				stopService();
		}
	};

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	public static void startService(Context context) {
		Intent service = new Intent(context, ManageProfilService.class);
		context.startService(service);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.e("[DEBUG ManageProfilService onStartCommand]", "onStartCommand");

		mRadiusResearch = PreferenceManager.getInstance(getApplicationContext()).getInt(PreferenceFields.RADIUS_PROFILE_RESEARCH);
		mDistMinToRefreshLocation = PreferenceManager.getInstance(getApplicationContext()).getInt(PreferenceFields.DIST_REFRESH_PROFILE_RESEARCH);
		mTimeMinRefreshLocation = PreferenceManager.getInstance(getApplicationContext()).getInt(PreferenceFields.TIME_REFRESH_PROFILE_RESEARCH);
		
		mUriNotification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		mProfilManager = ProfilManagement.getInstance(getApplicationContext());
		mManagerNotification = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

		mHandlerProfil = new Handler();
		mManagmentService.run();

		// TODO
		// Verify for the provider
		mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, mTimeMinRefreshLocation, 
				mDistMinToRefreshLocation, this);

		return START_STICKY;
	}

	private void stopService() {
		stopSelf();
	}

	private Boolean haveProfilsEnabled() {

		Boolean ret = false;
		Cursor cursor = getContentResolver().query(Profils.CONTENT_URI, Profils.PROJECTION, Profils.Columns.ACTIVE.getName() + "=?",
				new String[]{Integer.toString(1)}, null);
		if (cursor.getCount() > 0)
			ret = true;
		cursor.close();
		return ret;
	}

	private void applyProfil(Profil profil) {

		Toast.makeText(getApplicationContext(), R.string.success_message_apply_profil, Toast.LENGTH_SHORT).show();

		mProfilManager.putBooleanValue(Profils.Columns.IS_SET.getName(), true);
		mProfilManager.updateProfilValuesInDatabase(Profils.Columns.ID.getName() + "=?", new String[]{Integer.toString(profil.id)});

		mProfilManager.putBooleanValue(Profils.Columns.IS_SET.getName(), false);
		mProfilManager.updateProfilValuesInDatabase(Profils.Columns.ID.getName() + "!=?", new String[]{Integer.toString(profil.id)});

		profil.isSet = true;

		profil.applyProfil();
		this.sendNotificationProfilIsSet(profil);
	}

	private void sendNotificationProfilIsSet(Profil profil) {

		Integer drawable;
		switch (profil.mode) {
		case ProfilConstant.TYPE_SONNERIE:
			drawable = R.drawable.notif_sound;
			break;
		case ProfilConstant.TYPE_VIBREUR:
			drawable = R.drawable.notif_vibrate;
			break;
		case ProfilConstant.TYPE_SILENCE:
			drawable = R.drawable.notif_no_sound;
			break;
		default:
			drawable = R.drawable.notif_no_sound;
			break;
		}

		NotificationCompat.Builder notification = new NotificationCompat.Builder(this);
		notification.setSmallIcon(drawable);
		notification.setTicker(getString(R.string.notification_apply_profil_ticker_autoprofil));
		notification.setContentTitle(profil.name);
		notification.setContentText(getString(R.string.notification_apply_profil_contenttext_activate));
		notification.setPriority(NotificationCompat.PRIORITY_MAX);
		notification.setAutoCancel(true);

		Intent intentPending = new Intent(EditProfilActivity.ACTION_SEE_PROFIL);
		intentPending.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		intentPending.putExtra(EditProfilActivity.EXTRA_ID_PROFIL, profil.id);

		TaskStackBuilder stack = TaskStackBuilder.create(this);
		stack.addParentStack(ProfilsListActivity.class);
		stack.addNextIntent(intentPending);

		PendingIntent pending = stack.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

		notification.setContentIntent(pending);
		notification.setSound(mUriNotification);
		mManagerNotification.notify(0, notification.build());
	}

	@Override
	public void onLocationChanged(Location location) {
		Log.e("[DEBUG ManageProfilService onLocationChanged]", "onLocationChanged");
		List<ProfilFound> list = new ArrayList<ProfilFound>();

		Cursor cursor = getContentResolver().query(Profils.CONTENT_URI, Profils.PROJECTION, 
				Profils.Columns.ACTIVE.getName() + "=? AND " + Profils.Columns.IS_SET.getName() + "!=?",
				new String[]{Integer.toString(1), Integer.toString(1)}, null);
		while (cursor.moveToNext()) {

			Profil profil = Profil.createFromCursor(this, cursor);
			Location locProfil = new Location(LocationManager.NETWORK_PROVIDER);
			locProfil.setLatitude(Double.valueOf(profil.latitude));
			locProfil.setLongitude(Double.valueOf(profil.longitude));

			float dist = location.distanceTo(locProfil);
			if (dist < mRadiusResearch) {
				ProfilFound p = new ProfilFound();
				p.distance = dist;
				p.profil = profil;
				list.add(p);
			}
		}
		cursor.close();

		if (list.size() > 1) {
			Collections.sort(list);
		}
		if (list != null && list.size() > 0) {
			mProfilManager.clearValues();
			mProfilManager.putBooleanValue(Profils.Columns.IS_SET.getName(), false);
			mProfilManager.updateProfilValuesInDatabase(Profils.Columns.ID.getName() + "!=?", new String[]{Integer.toString(list.get(0).profil.id)});
			this.applyProfil(list.get(0).profil);
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.e("[DEBUG ManageProfilService onProviderDisabled]", "onProviderDisabled");
	}

	@Override
	public void onProviderEnabled(String provider) {
		Log.e("[DEBUG ManageProfilService onProviderEnabled]", "onProviderEnabled");
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		Log.e("[DEBUG ManageProfilService onStatusChanged]", "onStatusChanged");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mProfilManager.clearValues();
		mProfilManager.putBooleanValue(Profils.Columns.IS_SET.getName(), false);
		mProfilManager.updateProfilValuesInDatabase(null, null);

		mLocationManager.removeUpdates(this);
		mHandlerProfil.removeCallbacks(mManagmentService);
		Log.e("[DEBUG ManageProfilService onDestroy]", "onDestroy");
	}

	class ProfilFound implements Comparable<ProfilFound> {

		public Profil profil;
		public Float distance;

		@Override
		public int compareTo(ProfilFound another) {
			if (this.distance < another.distance)
				return -1;
			else if (this.distance > another.distance)
				return 1;
			else
				return 0;
		}
	}
}

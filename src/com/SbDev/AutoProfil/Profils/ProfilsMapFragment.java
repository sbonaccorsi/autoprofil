package com.SbDev.AutoProfil.Profils;

import java.util.ArrayList;
import java.util.List;

import android.app.WallpaperManager;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.Base.BaseMapFragment;
import com.SbDev.AutoProfil.Map.CustomMarkerFlag;
import com.SbDev.AutoProfil.Profils.Edit.EditProfilActivity;
import com.SbDev.AutoProfil.data.Local.DataInterface.IdCursorLoader;
import com.SbDev.AutoProfil.data.Local.DataInterface.PreferenceFields;
import com.SbDev.AutoProfil.data.Local.DataInterface.ProfilConstant;
import com.SbDev.AutoProfil.data.Local.MarkerData;
import com.SbDev.AutoProfil.data.provider.AutoProfilContent.Profils;
import com.bonadevapps.compoments.preferences.PreferenceManager;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

public class ProfilsMapFragment extends BaseMapFragment {

	public static final String						TAG_PROFILS_MAP_FRAGMENT = "mapPofilsFragment";

	private List<MarkerData>						mListMarker = new ArrayList<MarkerData>();

	public ProfilsMapFragment() {
		super();
	}

	public static ProfilsMapFragment newInstance() {

		ProfilsMapFragment map = new ProfilsMapFragment();
		return map;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		initCursorLoaders(IdCursorLoader.ID_LOADER_PROFIL_MAP);
		if (mMap != null) {
			mCustomFlag = new CustomMarkerFlag(getActivity());
			mMap.setInfoWindowAdapter(mCustomFlag);
			mMap.setOnMyLocationButtonClickListener(this);
			centerOnMyLocation();
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle param) {

		switch (id) {
		case IdCursorLoader.ID_LOADER_PROFIL_MAP:
			CursorLoader loader = new CursorLoader(getActivity().getApplicationContext(), Profils.CONTENT_URI, Profils.PROJECTION,
					null, null, null);
			return loader;

		default:
			return super.onCreateLoader(id, param);
		}
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		super.onLoadFinished(loader, cursor);

		switch (loader.getId()) {
		case IdCursorLoader.ID_LOADER_PROFIL_MAP:
			if (mMap != null) {
				if (mListMarker.size() >= cursor.getCount()) {
					mMap.clear();
					mListMarker.clear();
				}
				while (cursor.moveToNext()) {
					MarkerData newMarker = MarkerData.createFromCursor(getActivity().getApplicationContext(), cursor);
					if (mListMarker.size() > 0) {
						if (containMarker(newMarker.tag) == false) {
							newMarker.marker = mMap.addMarker(new MarkerOptions().title(newMarker.name)
									.position(newMarker.coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
							
							int radiusValue = PreferenceManager.getInstance(getActivity()).getInt(PreferenceFields.RADIUS_PROFILE_RESEARCH);
							Double radius = Double.valueOf(radiusValue);
							CircleOptions circleOption = createCircle(newMarker.coordinate, R.color.holo_green_light, R.color.holo_green_light_trans, radius);
							newMarker.circle = mMap.addCircle(circleOption);
							
							mListMarker.add(newMarker);
						}
					}
					else {
						newMarker.marker = mMap.addMarker(new MarkerOptions().title(newMarker.name)
								.position(newMarker.coordinate).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
						
						int radiusValue = PreferenceManager.getInstance(getActivity()).getInt(PreferenceFields.RADIUS_PROFILE_RESEARCH);
						Double radius = Double.valueOf(radiusValue);
						CircleOptions circleOption = createCircle(newMarker.coordinate, R.color.holo_green_light, R.color.holo_green_light_trans, radius);
						newMarker.circle = mMap.addCircle(circleOption);
						
						mListMarker.add(newMarker);
					}
				}
			}
			break;

		default:
			break;
		}
	}

	private Boolean containMarker(String tag) {

		Boolean ret = false;
		for (MarkerData data : mListMarker) {
			if (data.tag.contentEquals(tag))
				ret = true;
		}
		return ret;
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		super.onLoaderReset(loader);
	}

	@Override
	public void onInfoWindowClick(Marker marker) {
		super.onInfoWindowClick(marker);
		String tag = "" + marker.getPosition().latitude + marker.getPosition().longitude;

		for (MarkerData data : mListMarker) {
			if (data.tag.contentEquals(tag)) {
				Intent intent = new Intent(EditProfilActivity.ACTION_SEE_PROFIL);
				intent.putExtra(EditProfilActivity.EXTRA_ID_PROFIL, data.id);
				startActivity(intent);
			}
		}
	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		mCancelCenterUser = true;
		animateCameraKeepProp(marker.getPosition());
		preloadImageOnCustomMarker(marker);
		return true;
	}

	@Override
	protected void preloadImageOnCustomMarker(final Marker marker) {

		MarkerData data = MarkerData.createFromMarker(getActivity(), marker);
		View v = LayoutInflater.from(getActivity()).inflate(R.layout.custom_flag, null);

		if (data.imageId != ProfilConstant.ID_DEFAULT_ITEM) {
			ImageLoader.getInstance().displayImage("file://" + data.image, (ImageView)v.findViewById(R.id.imageView_profil_img_marker), new ImageLoadingListener() {
				@Override
				public void onLoadingStarted(String imageUri, View view) {
				}
				@Override
				public void onLoadingFailed(String imageUri, View view,
						FailReason failReason) {
				}
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					mCustomFlag.setCustomImage(loadedImage);
					marker.showInfoWindow();
				}
				@Override
				public void onLoadingCancelled(String imageUri, View view) {
				}
			});
		}
		else {
			WallpaperManager wall = WallpaperManager.getInstance(getActivity());
			Drawable draw = wall.getDrawable();
			mCustomFlag.setCustomImage(draw);
			marker.showInfoWindow();
		}
	}

	@Override
	public void onCameraChange(CameraPosition position) {
		mCancelCenterUser = true;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mMap != null) {
			mMap.setOnMyLocationButtonClickListener(null);
		}
	}
}

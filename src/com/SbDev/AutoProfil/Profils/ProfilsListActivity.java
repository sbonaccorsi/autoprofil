package com.SbDev.AutoProfil.Profils;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.Base.BaseFragmentActivity;
import com.SbDev.AutoProfil.Profils.Edit.EditProfilActivity;
import com.SbDev.AutoProfil.Service.ManageProfilService;
import com.SbDev.AutoProfil.Settings.SettingsActivity;
import com.SbDev.AutoProfil.data.Local.DataInterface.DefaultResearchValue;
import com.SbDev.AutoProfil.data.Local.DataInterface.PreferenceFields;
import com.SbDev.AutoProfil.data.provider.AutoProfilContent.Profils;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.bonadevapps.compoments.preferences.PreferenceManager;
import com.bonadevapps.utils.UtilsApplication;

public class ProfilsListActivity extends BaseFragmentActivity {

	private ProfilsMapFragment mProfilsMapFragment;

	@Override
	protected Integer getContentView() {
		return R.layout.activity_profils_list;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.e("Application info", "Is debuggable = " + UtilsApplication.isDebuggableMode(this));
		Log.e("Application info", "Is run on Emulator = " + UtilsApplication.isRunOnEmulator());
		
		this.initStartPreference();
		
		mProfilsMapFragment = (ProfilsMapFragment) getSupportFragmentManager().findFragmentByTag(ProfilsMapFragment.TAG_PROFILS_MAP_FRAGMENT);
		if (mProfilsMapFragment == null) {
			mProfilsMapFragment = ProfilsMapFragment.newInstance();
			getSupportFragmentManager().beginTransaction()
			.add(R.id.container_fragment, mProfilsMapFragment, ProfilsMapFragment.TAG_PROFILS_MAP_FRAGMENT)
			.hide(mProfilsMapFragment).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_list_profil, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = null;
		switch (item.getItemId()) {
		case R.id.action_create_profil:
			intent = new Intent(EditProfilActivity.ACTION_ADD_PROFIL);
			startActivity(intent);
			break;

		case R.id.action_map_profils:
			if (mProfilsMapFragment.isHidden()) {
				getSupportFragmentManager().beginTransaction().show(mProfilsMapFragment).commit();
				item.setIcon(R.drawable.collections_view_as_list);
			}
			else {
				getSupportFragmentManager().beginTransaction().hide(mProfilsMapFragment).commit();
				item.setIcon(R.drawable.location_map);
			}
			break;

		case R.id.action_settings:
			intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			break;

		case R.id.action_stop_all_profil:
			ContentValues values = new ContentValues();
			values.put(Profils.Columns.ACTIVE.getName(), false);
			values.put(Profils.Columns.IS_SET.getName(), false);
			getContentResolver().update(Profils.CONTENT_URI, values, null, null);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void initStartPreference() {
		PreferenceManager pref = PreferenceManager.getInstance(this);
		
		if (pref.containValue(PreferenceFields.START_PREFERENCES_IS_INIT) == false
				&& pref.getBoolean(PreferenceFields.START_PREFERENCES_IS_INIT) == false) {

			if (pref.containValue(PreferenceFields.ASK_CONFIRMATION_SET_PROFILE) == false) {
				pref.putBoolean(PreferenceFields.ASK_CONFIRMATION_SET_PROFILE, false);
			}
			
			if (pref.containValue(PreferenceFields.RADIUS_PROFILE_RESEARCH) == false) {
				pref.putInt(PreferenceFields.RADIUS_PROFILE_RESEARCH, DefaultResearchValue.DEFAULT_RADIUS_RESEARCH_VALUE);
			}
			
			if (pref.containValue(PreferenceFields.DIST_REFRESH_PROFILE_RESEARCH) == false) {
				pref.putInt(PreferenceFields.DIST_REFRESH_PROFILE_RESEARCH, DefaultResearchValue.DEFAULT_DIST_REFRESH_PROFILE_RESEARCH);
			}
			
			if (pref.containValue(PreferenceFields.TIME_REFRESH_PROFILE_RESEARCH) == false) {
				pref.putInt(PreferenceFields.TIME_REFRESH_PROFILE_RESEARCH, DefaultResearchValue.DEFAULT_TIME_REFRESH_PROFILE_RESEARCH);
			}
			
			pref.putBoolean(PreferenceFields.START_PREFERENCES_IS_INIT, true);
		}
	}
}

package com.SbDev.AutoProfil.Profils;

import yuku.iconcontextmenu.IconContextMenu;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.WallpaperManager;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.Base.BaseCursorListFragment;
import com.SbDev.AutoProfil.Dialog.DialogProfil;
import com.SbDev.AutoProfil.Profils.Edit.EditProfilActivity;
import com.SbDev.AutoProfil.Service.ManageProfilService;
import com.SbDev.AutoProfil.data.Local.DataInterface.IdCursorLoader;
import com.SbDev.AutoProfil.data.Local.DataInterface.ProfilConstant;
import com.SbDev.AutoProfil.data.Local.Profil;
import com.SbDev.AutoProfil.data.provider.AutoProfilContent.Profils;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.nostra13.universalimageloader.core.ImageLoader;

import eu.inmite.android.lib.dialogs.BaseDialogFragment;
import eu.inmite.android.lib.dialogs.SimpleDialogFragment;
import eu.inmite.android.lib.dialogs.SimpleDialogFragment.SimpleDialogBuilder;

public class ProfilsListFragment extends BaseCursorListFragment {

	private ImageLoader						mImageLoader = ImageLoader.getInstance();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_profils_list, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mAdapter = new CursorAdapter(getActivity(), R.layout.cell_profil_list, null, new String[]{}, new int[]{}, 0);
		mList.setAdapter(mAdapter);
		initCursorLoader(IdCursorLoader.ID_LOADER_PROFIL_LIST);
		//		registerForContextMenu(mList);
	}
	
	@Override
	protected void onClickItemList(AdapterView<?> adp, View view, int position, long id) {

		Cursor cursor = (Cursor) mAdapter.getItem(position);
		Profil profil = Profil.createFromCursor(getActivity().getApplicationContext(), cursor);

		Intent intent = new Intent(EditProfilActivity.ACTION_SEE_PROFIL);
		intent.putExtra(EditProfilActivity.EXTRA_ID_PROFIL, profil.id);
		startActivity(intent);
	}

	@Override
	protected boolean onLongClickItemList(AdapterView<?> adp, View view, int position, long id) {
		Cursor cursor = (Cursor) mAdapter.getItem(position);
		Profil profil = Profil.createFromCursor(getActivity(), cursor);
		DialogProfil.show(getSherlockActivity(), profil);
		//		mList.showContextMenu();
		return true;
	}

	// Overide context menu parent
	// With IconContextMenu for display icon in contextMenu
	@Override
	protected void onCreateContextMenu(IconContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		//		Profil profil = Profil.createFromCursor(getActivity(), mCursorItemClick);
		//		profil.buildContextMenuListProfil(getActivity(), menu);
	}

	@Override
	protected boolean onContextItemSelected(MenuItem item, Object info) {
		return super.onContextItemSelected(item, info);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle param) {

		switch (id) {
		case IdCursorLoader.ID_LOADER_PROFIL_LIST:
			CursorLoader loader = new CursorLoader(getActivity(), Profils.CONTENT_URI, Profils.PROJECTION, null, null, null);
			return loader;
		default:
			return super.onCreateLoader(id, param);
		}
	}

	public class ViewHolderClass {

		public TextView mName;
		public ImageView mImg;
		public ImageView mIconeMode;
		public ImageView mImgAct;

		public boolean isAnimate = false;

		public ViewHolderClass(View v) {
			mName = (TextView) v.findViewById(R.id.textView_cell_profil_name);
			//			mName.setTypeface(mFontSubTitle);
			mImgAct = (ImageView) v.findViewById(R.id.imageView_cell_profil_active);
			mImg = (ImageView) v.findViewById(R.id.imageView_cell_profile_image);
			mIconeMode = (ImageView) v.findViewById(R.id.imageView_cell_profil_mode);
		}
	}

	@Override
	public void initAdapterView(View v) {
		ViewHolderClass holder = (ViewHolderClass) v.getTag();
		if (holder == null) {
			ViewHolderClass hh = new ViewHolderClass(v);
			holder = hh;
			v.setTag(holder);
		}
	}

	@Override
	protected void configureView(View view, Cursor cursor) {

		if (cursor != null) {
			ViewHolderClass holder = (ViewHolderClass) view.getTag();

			Profil profil = Profil.createFromCursor(getActivity(), cursor);

			holder.mName.setText(profil.name);
			if (profil.backgroundPath != null && profil.backgroundPath.length() != 0
					&& profil.backgroundId != null && profil.backgroundId != ProfilConstant.ID_DEFAULT_ITEM) {
				mImageLoader.displayImage("file://" + profil.backgroundPath, holder.mImg);
			}
			else {
				holder.mImg.setImageDrawable(WallpaperManager.getInstance(getActivity()).getDrawable());
				//				holder.mImg.setImageResource(R.drawable.default_image_incline);
			}

			switch (profil.mode) {
			case ProfilConstant.TYPE_SONNERIE:
				holder.mIconeMode.setImageResource(R.drawable.sound);
				break;
			case ProfilConstant.TYPE_SILENCE:
				holder.mIconeMode.setImageResource(R.drawable.no_sound);
				break;
			case ProfilConstant.TYPE_VIBREUR:
				holder.mIconeMode.setImageResource(R.drawable.vibrate);
				break;
			default:
				break;
			}

			if (profil.active)
				holder.mImgAct.setImageResource(R.drawable.enable);
			else
				holder.mImgAct.setImageResource(R.drawable.disable);
		}
	}
}

package com.SbDev.AutoProfil.Profils.Edit;

import java.util.ArrayList;

import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.data.Local.DataInterface.IdCursorLoader;
import com.SbDev.AutoProfil.data.provider.AutoProfilContent.Profils;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore.Audio.Media;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;

public class SelectRingtoneFragment extends AbstractSelectSoundFragment {

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		
		mMedia_Cat1 = Media.IS_MUSIC;
		mMedia_Cat2 = Media.IS_RINGTONE;
		mUriMedia = Media.EXTERNAL_CONTENT_URI;
		
		mTabNavigation = new ArrayList<String>();
		mTabNavigation.add(getString(R.string.edit_profil_spinner_sound_music));
		mTabNavigation.add(getActivity().getString(R.string.edit_profil_spinner_sound_ringtone));
		
		SOUND_ID = Profils.Columns.RINGTONE_ID.getName();
		SOUND_DATA = Profils.Columns.RINGTONE_DATA.getName();
		SOUND_CATEGORY = Profils.Columns.RINGTONE_CATEGORY.getName();
		
		super.onViewCreated(view, savedInstanceState);
		
		mTitleRingtone.setText(getString(R.string.edit_profil_title_ringtone_call));
		
		getActivity().getSupportLoaderManager().initLoader(IdCursorLoader.ID_CURSOR_RINGTONE, null, this);
	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle param) {

		switch (id) {
		case IdCursorLoader.ID_CURSOR_RINGTONE:
			return new CursorLoader(getActivity().getApplicationContext(), mUriMedia,
					null, mCategory + "!=?", new String[]{Integer.toString(0)}, null);
		}
		return null;
	}
	
	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

		switch (loader.getId()) {
		case IdCursorLoader.ID_CURSOR_RINGTONE:
			mAdapter.changeCursor(cursor);
			break;
		}
	}
	
	@Override
	protected void updateCategorySelected(String category) {
		super.updateCategorySelected(category);
		getLoaderManager().restartLoader(IdCursorLoader.ID_CURSOR_RINGTONE, null, this);
	}
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		getActivity().getSupportLoaderManager().destroyLoader(IdCursorLoader.ID_CURSOR_RINGTONE);
	}
}

package com.SbDev.AutoProfil.Profils.Edit;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.Base.BaseFragmentActivity;
import com.SbDev.AutoProfil.Profils.ProfilsListActivity;
import com.SbDev.AutoProfil.Profils.Model.BranchPage;
import com.SbDev.AutoProfil.Profils.Model.Page;
import com.SbDev.AutoProfil.Profils.Model.StateNavigationObserver;
import com.SbDev.AutoProfil.Profils.Model.TreeRoot;
import com.SbDev.AutoProfil.Service.ManageProfilService;
import com.SbDev.AutoProfil.Ui.CustomViewPager;
import com.SbDev.AutoProfil.data.Local.DataInterface.ProfilConstant;
import com.SbDev.AutoProfil.data.Local.Profil;
import com.SbDev.AutoProfil.data.Local.ProfilManagement;
import com.SbDev.AutoProfil.data.provider.AutoProfilContent.Profils;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.bonadevapps.utils.UtilsActivity;
import com.bonadevapps.utils.UtilsApplication;

public class EditProfilActivity extends BaseFragmentActivity implements OnClickListener, 
OnPageChangeListener, StateNavigationObserver {

	public static final String					ACTION_SEE_PROFIL = "android.action.See";
	public static final String					ACTION_EDIT_PROFIL = "android.action.Edit";
	public static final String					ACTION_ADD_PROFIL = "android.action.Add";

	public static final String					EXTRA_ID_PROFIL = "profilID";

	private RecapProfilFragment					mRecapFragment;

	private ProfilManagement					mProfilManagement;

	private CustomViewPager 					mViewPager;
	private Button 								mButtonNext;
	private Button 								mButtonPrev;

	private Integer								mIdProfil;

	private TreeRoot 							mTree;

	private String								mAction;
	
	private SearchView							mSearchView;
	private MenuItem							mItemSearch;

	@Override
	protected Integer getContentView() {
		return R.layout.activity_edit_profil;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		mAction = getIntent().getAction();
		mProfilManagement = ProfilManagement.getInstance(getApplicationContext());

		if (mAction.contentEquals(ACTION_ADD_PROFIL)) {
			getSupportActionBar().setTitle(R.string.title_actionbar_create_profil);

			((FrameLayout)findViewById(R.id.container_fragment_see)).setVisibility(View.GONE);
			mProfilManagement.clearValues();
			this.initializeFormProfil();
		}
		else if (mAction.contentEquals(ACTION_SEE_PROFIL)) {

			getSupportActionBar().setTitle(R.string.title_actionbar_see_profil);
			
			mIdProfil = getIntent().getExtras().getInt(EXTRA_ID_PROFIL);

			((LinearLayout)findViewById(R.id.container_view_add_profil)).setVisibility(View.GONE);
			mRecapFragment = (RecapProfilFragment) getSupportFragmentManager().findFragmentByTag(RecapProfilFragment.TAG_FRAGMENT);
			if (mRecapFragment == null) {
				Bundle param = new Bundle();
				param.putString(RecapProfilFragment.EXTRA_GET_ACTION, RecapProfilFragment.ACTION_SEE_PROFIL);
				param.putInt(RecapProfilFragment.EXTRA_GET_PROFIL_ID, mIdProfil);
				mRecapFragment = (RecapProfilFragment) SherlockFragment.instantiate(getApplicationContext(), RecapProfilFragment.class.getName(), param);
				getSupportFragmentManager().beginTransaction().add(R.id.container_fragment_see, mRecapFragment, RecapProfilFragment.TAG_FRAGMENT).commit();
			}
		}
		else if (mAction.contentEquals(ACTION_EDIT_PROFIL)) {
			getSupportActionBar().setTitle(R.string.title_actionbar_edit_profil);

			((FrameLayout)findViewById(R.id.container_fragment_see)).setVisibility(View.GONE);
			mProfilManagement.clearValues();
			this.initializeEditProfil();
			this.initializeFormProfil();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (mAction.contentEquals(ACTION_EDIT_PROFIL)) {
			
			this.createItemMenuSearch(menu);
			
			getSupportMenuInflater().inflate(R.menu.menu_edit_profil, menu);
		}
		else if (mAction.contentEquals(ACTION_SEE_PROFIL)) {
			getSupportMenuInflater().inflate(R.menu.menu_see_profil, menu);
			Profil profil = Profil.createFromId(getApplicationContext(), mIdProfil);
			if (profil != null) {
				MenuItem item = menu.findItem(R.id.action_handle_active_profil);
				if (profil.active) {
					item.setIcon(R.drawable.av_stop);
				}
				else {
					item.setIcon(R.drawable.av_play);
				}
			}
		}
		else {
			this.createItemMenuSearch(menu);
		}
		return super.onCreateOptionsMenu(menu);
	}
	
	private void createItemMenuSearch(Menu menu) {
		mSearchView = new SearchView(getSupportActionBar().getThemedContext());
		
		mItemSearch = menu.add(R.string.title_action_search_address);
		mItemSearch.setIcon(R.drawable.action_search);
		mItemSearch.setActionView(mSearchView);
		mItemSearch.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		mItemSearch.setVisible(false);
		
		AutoCompleteTextView edit = (AutoCompleteTextView) mSearchView.findViewById(R.id.abs__search_src_text);
		edit.setTextColor(getResources().getColor(android.R.color.white));
		edit.setHintTextColor(getResources().getColor(android.R.color.white));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		ContentValues values = null;

		switch (item.getItemId()) {
		case R.id.action_validate_edit_profil:
			String error = isValidProfilForUpdate(mProfilManagement); 
			if (error == null) {
				Integer id = (Integer) mProfilManagement.getValue(Profils.Columns.ID.getName());
				Integer mode = (Integer) mProfilManagement.getValue(Profils.Columns.MODE.getName());
				if (mode != null && mode != ProfilConstant.TYPE_SONNERIE) {
					mProfilManagement.putIntegerValue(Profils.Columns.RINGTONE_ID.getName(), null);
					mProfilManagement.putStringValue(Profils.Columns.RINGTONE_DATA.getName(), null);
					mProfilManagement.putStringValue(Profils.Columns.RINGTONE_CATEGORY.getName(), null);
					mProfilManagement.putIntegerValue(Profils.Columns.NOTIFICATION_RINGTONE_LEVEL.getName(), null);
					mProfilManagement.putStringValue(Profils.Columns.NOTIFICATION_CATEGORY.getName(), null);
					mProfilManagement.putStringValue(Profils.Columns.NOTIFICATION_DATA.getName(), null);
					mProfilManagement.putIntegerValue(Profils.Columns.NOTIFICATION_ID.getName(), null);
				}

				mProfilManagement
				.updateProfilValuesInDatabase(Profils.Columns.ID.getName() + "=?", new String[]{Integer.toString(id)});
				Toast.makeText(this, R.string.success_message_update_profil, Toast.LENGTH_SHORT).show();
				finish();
				return true;
			}
			else {
				Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
			}
			return false;

		case R.id.action_handle_active_profil:
			if (mAction.contentEquals(ACTION_SEE_PROFIL)) {
				Profil profil = Profil.createFromId(getApplicationContext(), mIdProfil);
				if (profil != null) {
					if (profil.active) {
						values = new ContentValues();
						values.put(Profils.Columns.ACTIVE.getName(), false);
						values.put(Profils.Columns.IS_SET.getName(), false);
						profil.updateProfil(values);
						item.setIcon(R.drawable.av_play);
					}
					else {
						values = new ContentValues();
						values.put(Profils.Columns.ACTIVE.getName(), true);
						profil.updateProfil(values);
						item.setIcon(R.drawable.av_stop);
						ManageProfilService.startService(getBaseContext());
					}
				}
			}
			break;

		case R.id.action_see_edit_profil:
			Profil.createFromId(this, mIdProfil).editProfil();
			finish();
			break;
			
		case R.id.action_see_delete_profil:
			Profil.createFromId(this, mIdProfil).deleteProfil();
			finish();
			break;
			
		case android.R.id.home:
			//			finish();
			boolean ret = UtilsActivity.activityIsRunning(getBaseContext(), ProfilsListActivity.class.getName());
			if (ret == true) {
				finish();
			}	
			else {
				Intent intent = new Intent(getBaseContext(), ProfilsListActivity.class);
				startActivity(intent);
			}
			break;

		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	private String isValidProfilForUpdate(ProfilManagement pm) {
		String error = "";
		Integer mode = (Integer) pm.getValue(Profils.Columns.MODE.getName());
		Integer wallpaperId = (Integer) pm.getValue(Profils.Columns.BACKGROUND_ID.getName());

		if (mode != null && mode == ProfilConstant.TYPE_SONNERIE) {

			List<String> errors = new ArrayList<String>();

			Integer idRing = (Integer) pm.getValue(Profils.Columns.RINGTONE_ID.getName());
			Integer idNotif = (Integer) pm.getValue(Profils.Columns.NOTIFICATION_ID.getName());

			if (idRing == null || idRing == 0)
				errors.add(getString(R.string.error_message_missing_param_ringtone));
			if (idNotif == null || idNotif == 0)
				errors.add(getString(R.string.error_message_missing_param_notification));
			if (wallpaperId == null || wallpaperId == 0)
				errors.add(getString(R.string.error_message_missing_param_wallpaper));

			if (errors.size() == 0) {
				return null;
			}
			else {
				if (errors.size() > 1)
					error += getString(R.string.error_message_missing_parameters) + " ";
				else
					error += getString(R.string.error_message_missing_parameter) + " ";
			
				for (int idx = 0; idx < errors.size(); ++idx) {
					error += errors.get(idx);
					if (idx != errors.size() - 1) {
						error += ", ";
					}
				}
				return error;
			}
		}
		if (wallpaperId != null && wallpaperId != 0) {
			return null;
		}
		return error + " " + getString(R.string.error_message_missing_param_wallpaper);
	}

	private void initializeEditProfil() {

		Integer idProfil = getIntent().getExtras().getInt(EXTRA_ID_PROFIL);
		Profil profil = Profil.createFromId(getBaseContext(), idProfil);
		mProfilManagement.createFromProfil(profil);
	}

	private void initializeFormProfil() {

		mViewPager = (CustomViewPager) findViewById(R.id.viewPager);
		mViewPager.setStateEnable(false);
		mViewPager.setOnPageChangeListener(this);

		mButtonNext = (Button) findViewById(R.id.next_button);
		mButtonPrev = (Button) findViewById(R.id.prev_button);
		mButtonNext.setOnClickListener(this);
		mButtonPrev.setOnClickListener(this);

		mTree = new TreeRoot(getApplicationContext(), mViewPager, getSupportFragmentManager());
		mTree.createTree(new Page(getApplicationContext(), SelectGeneralModeFragment.class.getName(), this, false, null),
				new BranchPage(getApplicationContext()).addPageToBranch(
						new Page(getApplicationContext(), SelectRingtoneFragment.class.getName(), this, true, null),
						new Page(getApplicationContext(), SelectNotificationFragment.class.getName(), this, true, null),
						new Page(getApplicationContext(), SelectSoundParamFragment.class.getName(), this, false, null),
						new Page(getApplicationContext(), SelectBrightnessFragment.class.getName(), this, false, null),
						new Page(getApplicationContext(), SelectBackgroundFragment.class.getName(), this, true, null),
						new Page(getApplicationContext(), SelectConnectivityParamFragment.class.getName(), this, false, null),
						new Page(getApplicationContext(), SelectLocalizationProfilFragment.class.getName(), this, 
								UtilsApplication.isRunOnEmulator() ? false : true, null),
						new Page(getApplicationContext(), RecapProfilFragment.class.getName(), this, true, null)),
						new BranchPage(getApplicationContext()).addPageToBranch(
								new Page(getApplicationContext(), SelectBrightnessFragment.class.getName(), this, false, null),
								new Page(getApplicationContext(), SelectBackgroundFragment.class.getName(), this, true, null),
								new Page(getApplicationContext(), SelectConnectivityParamFragment.class.getName(), this, false, null),
								new Page(getApplicationContext(), SelectLocalizationProfilFragment.class.getName(), this, 
										UtilsApplication.isRunOnEmulator() ? false : true, null),
								new Page(getApplicationContext(), RecapProfilFragment.class.getName(), this, true, null)),
								new BranchPage(getApplicationContext()).addPageToBranch(
										new Page(getApplicationContext(), SelectBrightnessFragment.class.getName(), this, false, null),
										new Page(getApplicationContext(), SelectBackgroundFragment.class.getName(), this, true, null),
										new Page(getApplicationContext(), SelectConnectivityParamFragment.class.getName(), this, false, null),
										new Page(getApplicationContext(), SelectLocalizationProfilFragment.class.getName(), this, 
												UtilsApplication.isRunOnEmulator() ? false : true, null),
										new Page(getApplicationContext(), RecapProfilFragment.class.getName(), this, true, null)));

	}

	@Override
	protected void onResume() {
		super.onResume();

		if (mAction.contentEquals(ACTION_SEE_PROFIL)) {
			mRecapFragment.initView(getBaseContext());
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mAction.contentEquals(ACTION_ADD_PROFIL)) {
			mButtonNext.setOnClickListener(null);
			mButtonPrev.setOnClickListener(null);
			mViewPager.setOnPageChangeListener(null);
			mViewPager = null;
			mButtonNext = null;
			mButtonPrev = null;
		}
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.next_button:
			mTree.nextPage();
			break;

		case R.id.prev_button:
			mTree.prevPage();
			break;
		}
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
	}
	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
	}

	@Override
	public void onPageSelected(int position) {

		if (position > 0)
			mButtonPrev.setEnabled(true);
		else {
			mButtonPrev.setEnabled(false);
			mButtonNext.setEnabled(true);
		}
	}

	@Override
	public void enableNextButton(boolean complete) {
		mButtonNext.setEnabled(complete);
	}
	
	@Override
	public void renameNextButton(String name) {
		mButtonNext.setText(name);
	}
	
	public void displaySearchView(boolean visibility) {
		mItemSearch.setVisible(visibility);
		if (visibility == false) {
			this.collapseActionViewItemSearch();
		}
	}
	
	public void collapseActionViewItemSearch() {
		mItemSearch.collapseActionView();
		mSearchView.setQuery("", false);
	}
	
	public SearchView getSearchView() {
		return mSearchView;
	}
}

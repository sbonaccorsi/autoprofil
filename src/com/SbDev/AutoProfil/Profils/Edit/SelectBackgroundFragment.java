package com.SbDev.AutoProfil.Profils.Edit;

import java.util.ArrayList;
import java.util.List;

import android.app.WallpaperManager;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.Dialog.DialogBackground;
import com.SbDev.AutoProfil.Profils.Model.PageFragment;
import com.SbDev.AutoProfil.data.Local.DataInterface.ProfilConstant;
import com.SbDev.AutoProfil.data.Local.ProfilManagement;
import com.SbDev.AutoProfil.data.Local.DataInterface.IdCursorLoader;
import com.SbDev.AutoProfil.data.provider.AutoProfilContent.Profils;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.nostra13.universalimageloader.core.ImageLoader;

public class SelectBackgroundFragment extends PageFragment implements LoaderCallbacks<Cursor>, OnItemLongClickListener, OnItemClickListener {

	private ActionBar						mActionBar;

	private String[]						mTabNavigation = {"", ""};

	private ImageBackAdapter				mAdapter;
	private ImageLoader						mImageLoader = ImageLoader.getInstance();

	private GridView						mGridView;

	private String							mOrderGrid;

	private List<Integer>					mCheckIds = new ArrayList<Integer>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_select_background, container, false);
		mGridView = (GridView) view.findViewById(R.id.gridView_image_gallery);
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mTabNavigation[0] = getString(R.string.edit_profil_spinner_background_newest);
		mTabNavigation[1] = getString(R.string.edit_profil_spinner_background_oldest);
		
		Integer idImg = (Integer) ProfilManagement.getInstance(getActivity().getApplicationContext()).getValue(Profils.Columns.BACKGROUND_ID.getName());
		if (idImg != null) {
			mCheckIds.add(idImg);
		}

		mAdapter = new ImageBackAdapter(getActivity(), R.layout.cell_background_select, null, new String[]{}, new int[]{}, 0);
		mGridView.setAdapter(mAdapter);
		mGridView.setOnItemLongClickListener(this);
		mGridView.setOnItemClickListener(this);
		mGridView.setFastScrollEnabled(true);

		mOrderGrid = ProfilConstant.ORDER_DESC;
		getActivity().getSupportLoaderManager().initLoader(IdCursorLoader.ID_LOADER_IMAGE_BACKGROUND, null, this);
	}

	@Override
	protected void enterThePage() {

		mActionBar = ((SherlockFragmentActivity)getActivity()).getSupportActionBar();
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

		ArrayAdapter<String> adp = new ArrayAdapter<String>(mActionBar.getThemedContext(), R.layout.custom_spinner_item, mTabNavigation);
		adp.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
		mActionBar.setListNavigationCallbacks(adp, new OnNavigationListener() {

			@Override
			public boolean onNavigationItemSelected(int itemPosition, long itemId) {
				switch (itemPosition) {
				case 0:
					mOrderGrid = ProfilConstant.ORDER_DESC;
					break;

				case 1:
					mOrderGrid = ProfilConstant.ORDER_ASC;
					break;
				}
				getActivity().getSupportLoaderManager().restartLoader(IdCursorLoader.ID_LOADER_IMAGE_BACKGROUND, null, SelectBackgroundFragment.this);
				return true;
			}
		});

		if (mIsForced && ProfilManagement.getInstance(getActivity().getApplicationContext()).getValue(Profils.Columns.BACKGROUND_ID.getName()) == null) {
			mListenerObserver.enableNextButton(false);
		}
		else if (ProfilManagement.getInstance(getActivity().getApplicationContext()).getValue(Profils.Columns.BACKGROUND_ID.getName()) != null)
			mListenerObserver.enableNextButton(true);
	}

	@Override
	protected void outOfPage() {
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle param) {

		switch (id) {
		case IdCursorLoader.ID_LOADER_IMAGE_BACKGROUND:
			return new CursorLoader(getActivity(), Media.EXTERNAL_CONTENT_URI, null, null, null, Media.DATE_TAKEN + " " + mOrderGrid);

		default:
			break;
		}
		return null;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

		switch (loader.getId()) {
		case IdCursorLoader.ID_LOADER_IMAGE_BACKGROUND:
			mAdapter.swapCursor(cursor);
			break;

		default:
			break;
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		mAdapter.swapCursor(null);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> adp, View view, int position,
			long id) {
		ViewHolderImageBack holder = (ViewHolderImageBack) view.getTag();
		DialogBackground.show((SherlockFragmentActivity)getActivity(), getString(R.string.title_dialog_background), holder.mData);

		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> adp, View view, int position, long id) {

		ViewHolderImageBack holder = (ViewHolderImageBack) view.getTag();
		View high = view.findViewById(R.id.imageView_select_validate);
		if (high.getVisibility() == View.GONE) {
			mCheckIds.clear();
			mCheckIds.add(holder.mId);
			callbackStateNextButton(true);
			ProfilManagement.getInstance(getActivity().getApplicationContext())
			.putIntegerValue(Profils.Columns.BACKGROUND_ID.getName(), holder.mId);
			ProfilManagement.getInstance(getActivity().getApplicationContext())
			.putStringValue(Profils.Columns.BACKGROUND_DATA.getName(), holder.mData);
		}
		else {
			ProfilManagement.getInstance(getActivity().getApplicationContext()).removeValue(Profils.Columns.BACKGROUND_ID.getName());
			ProfilManagement.getInstance(getActivity().getApplicationContext()).removeValue(Profils.Columns.BACKGROUND_DATA.getName());
			mCheckIds.clear();
			callbackStateNextButton(false);
		}
		mAdapter.notifyDataSetChanged();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

	public class ViewHolderImageBack {

		public ImageView mImg;
		public ImageView mValidate;

		public Integer mId;
		public String mData;

		public ViewHolderImageBack(View v) {

			mImg = (ImageView) v.findViewById(R.id.imageView_select_back);
			mValidate = (ImageView) v.findViewById(R.id.imageView_select_validate);
		}
	}

	public class ImageBackAdapter extends SimpleCursorAdapter {

		public ImageBackAdapter(Context context, int layout, Cursor c,
				String[] from, int[] to, int flags) {
			super(context, layout, c, from, to, flags);
		}

		@Override
		public int getCount() {
			return super.getCount() + 1;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup viewGroup) {
			View view = null;

			if (position == 0) {
				view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_background_select, null);
			}
			else {
				view = super.getView(position - 1, convertView, viewGroup);
			}

			final ViewHolderImageBack holder;
			if (view.getTag() == null) {
				ViewHolderImageBack h = new ViewHolderImageBack(view);
				view.setTag(h);
				holder = h;
			}
			else
				holder = (ViewHolderImageBack) view.getTag();

			if (position == 0) {
				holder.mId = ProfilConstant.ID_DEFAULT_ITEM;
				WallpaperManager wall = WallpaperManager.getInstance(getActivity());
				if (wall.getDrawable() != null) {
					holder.mImg.setImageDrawable(wall.getDrawable());
				}
				else {
					holder.mImg.setImageResource(R.drawable.default_image_incline);
				}
			}
			else {
				final Cursor cursor = getCursor();
				cursor.moveToPosition(position - 1);

				int indexColId = cursor.getColumnIndex(Media._ID);
				int indexColData = cursor.getColumnIndex(Media.DATA);

				holder.mId = Integer.valueOf(cursor.getInt(indexColId));
				holder.mData = cursor.getString(indexColData);
				mImageLoader.displayImage("file://" + cursor.getString(indexColData), holder.mImg);
			}

			if (mCheckIds.size() != 0) {
				if (holder.mId != null) {
					if (mCheckIds.get(0).longValue() == holder.mId.longValue()) {
						holder.mValidate.setVisibility(View.VISIBLE);
					}
					else {
						holder.mValidate.setVisibility(View.GONE);
					}
				}
			}
			else {
				holder.mValidate.setVisibility(View.GONE);
			}
			return view;
		}
	}
}

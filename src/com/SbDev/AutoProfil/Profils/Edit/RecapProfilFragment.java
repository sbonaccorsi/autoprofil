package com.SbDev.AutoProfil.Profils.Edit;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.WallpaperManager;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore.Audio.Media;
import android.provider.Settings.SettingNotFoundException;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.Profils.Model.PageFragment;
import com.SbDev.AutoProfil.Service.ManageProfilService;
import com.SbDev.AutoProfil.data.Local.DataInterface.ProfilConstant;
import com.SbDev.AutoProfil.data.Local.Profil;
import com.SbDev.AutoProfil.data.Local.ProfilManagement;
import com.SbDev.AutoProfil.data.provider.AutoProfilContent.Profils;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.bonadevapps.utils.UtilsGeocoder;
import com.nostra13.universalimageloader.core.ImageLoader;

public class RecapProfilFragment extends PageFragment implements TextWatcher {

	public static final String			EXTRA_GET_ACTION = "action";
	public static final String			EXTRA_GET_PROFIL_ID = "profilID";

	public static final String			ACTION_SEE_PROFIL = "android.action.see";
	public static final String			TAG_FRAGMENT = "RecapProfilFragment";

	private ActionBar					mActionBar;

	private Boolean						mIsEditMode = false;
	private Integer						mProfilId;

	private ActionMode					mActionMode;
	private ImageLoader					mImageLoader = ImageLoader.getInstance();

	private ProfilManagement			mProfilManagement;

	private View						mIncludeSound;

	private TextView					mTitleRingtone;
	private SeekBar						mVolumeRingtoneSeekBar;

	private TextView					mTitleNotification;

	private SeekBar						mVolumeSoundSeekBar;

	private TextView					mIntensityBrightness;
	private SeekBar						mSeekBarIntensityBrightness;

	private ImageView					mImageBackground;

	private CheckBox					mCheckWifi;
	private CheckBox					mCheckBluethoot;

	private TextView					mAddress;

	private View						mCustomViewActionMode;
	//	private EditText					mNameEdit;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_recap_profil, container, false);
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mProfilManagement = ProfilManagement.getInstance(getActivity().getApplicationContext());

		mIncludeSound = view.findViewById(R.id.container_ringtone_ref);
		mTitleRingtone = (TextView) view.findViewById(R.id.textView_recap_title_ringtone);
		mTitleNotification = (TextView) view.findViewById(R.id.textView_recap_title_notification);
		mVolumeRingtoneSeekBar = (SeekBar) view.findViewById(R.id.seekBar_recap_volume_ringtone);
		mVolumeSoundSeekBar = (SeekBar) view.findViewById(R.id.seekBar_recap_volume_sound);

		mVolumeRingtoneSeekBar.setEnabled(false);
		mVolumeSoundSeekBar.setEnabled(false);

		AudioManager audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
		int maxVolumeRingtone = audioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
		int maxVolumeSound = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		mVolumeRingtoneSeekBar.setMax(maxVolumeRingtone);
		mVolumeSoundSeekBar.setMax(maxVolumeSound);

		mIntensityBrightness = (TextView) view.findViewById(R.id.textView_recap_intensity_bright);
		mSeekBarIntensityBrightness = (SeekBar) view.findViewById(R.id.seekBar_recap_brightness_intensity);
		mSeekBarIntensityBrightness.setMax(255);
		mSeekBarIntensityBrightness.setEnabled(false);

		mImageBackground = (ImageView) view.findViewById(R.id.imageView_recap_background);

		mCheckWifi = (CheckBox) view.findViewById(R.id.checkbox_recap_wifi);
		mCheckBluethoot = (CheckBox) view.findViewById(R.id.checkbox_recap_bluethoot);
		mCheckWifi.setText(R.string.recap_wifi);
		mCheckBluethoot.setText(R.string.recap_bluethoot);

		mAddress = (TextView) view.findViewById(R.id.textView_recap_address);

		//		mNameEdit = (EditText) view.findViewById(R.id.editText_recap_name_profil);
		//		mNameEdit.addTextChangedListener(this);
	}

	private void initRecapRingtone() {

		Integer idRingtone = (Integer) mProfilManagement.getValue(Profils.Columns.RINGTONE_ID.getName());
		if (idRingtone != null) {
			if (idRingtone != ProfilConstant.ID_DEFAULT_ITEM) {
				Integer category = (Integer) mProfilManagement.getValue(Profils.Columns.RINGTONE_CATEGORY.getName());
				Uri uri = this.getMediaSoundUriByCategory(category);
				Cursor cursor = getActivity().getContentResolver().query(uri, null,
						Media._ID + "=?", new String[]{Long.toString(idRingtone)}, null);
				if (cursor.moveToFirst()) {
					mTitleRingtone.setText(getString(R.string.recap_title_phone_ringtone) + cursor.getString(cursor.getColumnIndex(Media.TITLE)));
				}
				cursor.close();
			}
			else {

				String name = this.getRingtoneDefaultRingtoneName(RingtoneManager.TYPE_RINGTONE);
				mTitleRingtone.setText(getString(R.string.recap_title_phone_ringtone) + name);
			}
			Integer volume = (Integer) mProfilManagement.getValue(Profils.Columns.NOTIFICATION_RINGTONE_LEVEL.getName());
			if (volume != null) {
				mVolumeRingtoneSeekBar.setProgress(volume);
			}
			this.initRecapNotification();
			this.initSoundVolume();
			this.initOtherParamSound();
		}
		else
			mIncludeSound.setVisibility(View.GONE);
	}

	private void initRecapNotification() {

		Integer idNotification = (Integer) mProfilManagement.getValue(Profils.Columns.NOTIFICATION_ID.getName());

		if (idNotification != null) {
			if (idNotification != ProfilConstant.ID_DEFAULT_ITEM) {
				Integer category = (Integer) mProfilManagement.getValue(Profils.Columns.NOTIFICATION_CATEGORY.getName());
				Uri uri = this.getMediaSoundUriByCategory(category);
				Cursor cursor = getActivity().getContentResolver().query(uri, null,
						Media._ID + "=?", new String[]{Long.toString(idNotification)}, null);
				if (cursor.moveToFirst()) {
					mTitleNotification.setText(getString(R.string.recap_title_notification_sound) + cursor.getString(cursor.getColumnIndex(Media.TITLE)));
				}
				cursor.close();
			}
			else {
				String name = this.getRingtoneDefaultRingtoneName(RingtoneManager.TYPE_NOTIFICATION);
				mTitleNotification.setText(getString(R.string.recap_title_notification_sound) + name);
			}
		}
	}

	private void initOtherParamSound() {
		//		((CheckBox)getView().findViewById(R.id.checkBox_recap_vibrate_when_ringing))
		Boolean value = null;

		value = (Boolean) mProfilManagement.getValue(Profils.Columns.DIAL_PAD_TONES.getName());
		if (value != null) {
			((CheckBox)getView().findViewById(R.id.checkBox_recap_dial_pad_touch_tones)).setChecked(value);
		}
		else {
			getView().findViewById(R.id.container_recap_dial_pad_touch_tones).setVisibility(View.GONE);
		}

		value = (Boolean) mProfilManagement.getValue(Profils.Columns.TOUCH_SOUNDS.getName());
		if (value != null) {
			((CheckBox)getView().findViewById(R.id.checkBox_recap_touch_sounds)).setChecked(value);
		}
		else {
			getView().findViewById(R.id.container_recap_touch_sounds).setVisibility(View.GONE);
		}

		value = (Boolean) mProfilManagement.getValue(Profils.Columns.SCREEN_LOCK_SOUNDS.getName());
		if (value != null) {
			((CheckBox)getView().findViewById(R.id.checkBox_recap_screen_lock_sounds)).setChecked(value);
		}
		else {
			getView().findViewById(R.id.container_recap_screen_lock_sounds).setVisibility(View.GONE);
		}

		value = (Boolean) mProfilManagement.getValue(Profils.Columns.VIBRATE_TOUCH.getName());
		if (value != null) {
			((CheckBox)getView().findViewById(R.id.checkBox_recap_vibrate_on_touch)).setChecked(value);
		}
		else {
			getView().findViewById(R.id.container_recap_vibrate_on_touch).setVisibility(View.GONE);
		}
	}

	private void initSoundVolume() {
		Integer volume = (Integer) mProfilManagement.getValue(Profils.Columns.SOUND_LEVEL.getName());
		if (volume != null) {
			mVolumeSoundSeekBar.setProgress(volume);
		}
	}

	private void initRecapDisplay() {

		Integer brightMode = (Integer) mProfilManagement.getValue(Profils.Columns.BRIGTNESS_MODE.getName());
		if (brightMode == ProfilConstant.TYPE_BRIGTNESS_AUTO) {
			((CheckBox)getView().findViewById(R.id.checkBox_recap_brightness_auto)).setChecked(true);
			mIntensityBrightness.setVisibility(View.GONE);
		}
		else if (brightMode == ProfilConstant.TYPE_BRIGTNESS_MANUAL) {
			Integer intensity = (Integer) mProfilManagement.getValue(Profils.Columns.BRIGTNESS_INTENSITY.getName());
			((CheckBox)getView().findViewById(R.id.checkBox_recap_brightness_auto)).setChecked(false);
			if (intensity != null) {
				mIntensityBrightness.setText(getString(R.string.recap_brightness_intensity));
				mSeekBarIntensityBrightness.setProgress(intensity);
			}
		}

		Integer idBack = (Integer) mProfilManagement.getValue(Profils.Columns.BACKGROUND_ID.getName());
		if (idBack != null && idBack != ProfilConstant.ID_DEFAULT_ITEM) {
			Cursor cursor = getActivity().getContentResolver().query(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
					null, android.provider.MediaStore.Images.Media._ID + "=?", new String[]{Integer.toString(idBack)}, null);
			if (cursor.moveToFirst()) {
				String data = cursor.getString(cursor.getColumnIndex(android.provider.MediaStore.Images.Media.DATA));
				mImageLoader.displayImage("file://" + data, mImageBackground);

			}
			cursor.close();
		}
		else {
			mImageBackground.setImageDrawable(WallpaperManager.getInstance(getActivity()).getDrawable());
		}

		Integer t = (Integer) mProfilManagement.getValue(Profils.Columns.SLEEP_TIME_SCREEN.getName());
		String time = getStringByTimeSleep(t);
		((TextView) getView().findViewById(R.id.textView_recap_sleep_time)).setText(getString(R.string.recap_sleep_after_first_part) + time + 
				getString(R.string.recap_sleep_after_second_part));
	}

	private void initRecapMoreOptions() {

		Boolean wifi = (Boolean) mProfilManagement.getValue(Profils.Columns.WIFI.getName());
		Boolean bluethoot = (Boolean) mProfilManagement.getValue(Profils.Columns.BLUETHOOT.getName());

		if (wifi == null || wifi == false)
			mCheckWifi.setChecked(false);
		else
			mCheckWifi.setChecked(true);
		if (bluethoot == null || bluethoot == false)
			mCheckBluethoot.setChecked(false);
		else
			mCheckBluethoot.setChecked(true);
	}

	private void initRecapPosition() {

		mAddress.setText("");

		String latitude = (String) ProfilManagement.getInstance(getActivity().getApplicationContext()).getValue(Profils.Columns.LATITUDE.getName());
		String longitude = (String) ProfilManagement.getInstance(getActivity().getApplicationContext()).getValue(Profils.Columns.LONGITUDE.getName());
		String addr = (String) ProfilManagement.getInstance(getActivity().getApplicationContext()).getValue(Profils.Columns.ADDRESS.getName());

		if (addr != null && TextUtils.isEmpty(addr) == false) {
			mAddress.setText(addr);
		}
		else {	
			if (latitude != null && latitude.length() != 0 &&
					longitude != null && longitude.length() != 0) {
				Double lat = Double.valueOf(latitude);
				Double lon = Double.valueOf(longitude);
				if (lat != null && lon != null) {
					AsyncLocalization task = new AsyncLocalization();
					task.execute(lat, lon);
				}
			}
		}
	}

	private String getStringByTimeSleep(int time) {
		switch (time) {
		case ProfilConstant.SLEEP_TIME_15000:
			return getResources().getString(R.string.time_sleep_15_seconds);
		case ProfilConstant.SLEEP_TIME_30000:
			return getResources().getString(R.string.time_sleep_30_seconds);
		case ProfilConstant.SLEEP_TIME_60000:
			return getResources().getString(R.string.time_sleep_1_minute);
		case ProfilConstant.SLEEP_TIME_120000:
			return getResources().getString(R.string.time_sleep_2_minutes);
		case ProfilConstant.SLEEP_TIME_360000:
			return getResources().getString(R.string.time_sleep_5_minutes);
		case ProfilConstant.SLEEP_TIME_720000:
			return getResources().getString(R.string.time_sleep_10_minutes);
		case ProfilConstant.SLEEP_TIME_2160000:
			return getResources().getString(R.string.time_sleep_30_minutes);
		}
		return null;
	}

	private Uri getMediaSoundUriByCategory(Integer category) {
		Uri uri = null;
		if (category != null && category == ProfilConstant.CATEGORY_MUSIC) {
			uri = Media.EXTERNAL_CONTENT_URI;
		}
		else if (category != null && category == ProfilConstant.CATEGORY_RINGTONE) {
			uri = Media.INTERNAL_CONTENT_URI;
		}
		return uri;
	}

	private String getRingtoneDefaultRingtoneName(int type) {
		Uri uri = RingtoneManager.getActualDefaultRingtoneUri(getActivity(), type);
		Ringtone ring = RingtoneManager.getRingtone(getActivity(), uri);
		if (ring != null) {
			return ring.getTitle(getActivity());
		}
		else {
			return "No ringtone";
		}
	}

	public void initView(Context ctx) {

		if (getArguments().containsKey(EXTRA_GET_ACTION) && 
				getArguments().getString(EXTRA_GET_ACTION).contentEquals(ACTION_SEE_PROFIL)) {
			mProfilId = getArguments().getInt(EXTRA_GET_PROFIL_ID);
			Profil profil = Profil.createFromId(ctx, mProfilId);

			//			mNameEdit.setVisibility(View.GONE);

			if (getActivity() instanceof SherlockFragmentActivity) {
				((SherlockFragmentActivity)getActivity()).getSupportActionBar().setTitle(profil.name);
			}

			// Set param brightness
			if (profil.brightnessMode == ProfilConstant.TYPE_BRIGTNESS_AUTO) {
				((CheckBox)getView().findViewById(R.id.checkBox_recap_brightness_auto)).setChecked(true);
				mIntensityBrightness.setVisibility(View.GONE);
			}
			else if (profil.brightnessMode == ProfilConstant.TYPE_BRIGTNESS_MANUAL) {
				((CheckBox)getView().findViewById(R.id.checkBox_recap_brightness_auto)).setChecked(false);
				if (profil.brightnessIntensity != null) {
					mIntensityBrightness.setText(getString(R.string.recap_brightness_intensity));
					mSeekBarIntensityBrightness.setProgress(profil.brightnessIntensity);
				}
			}

			// Set param sleep time off
			Integer t = profil.sleepTimeScreen;
			String time = getStringByTimeSleep(t);
			((TextView) getView().findViewById(R.id.textView_recap_sleep_time)).setText(getString(R.string.recap_sleep_after_first_part) + time + 
					getString(R.string.recap_sleep_after_second_part));

			// Set param ringtone
			if (profil.mode == ProfilConstant.TYPE_SONNERIE) {
				Integer category = null;
				Cursor cursor = null;
				Uri uri = null;
				if (profil.ringtoneId != null && profil.ringtoneId != ProfilConstant.ID_DEFAULT_ITEM) {
					category = profil.ringtoneCategory;
					uri = this.getMediaSoundUriByCategory(category);
					cursor = ctx.getContentResolver().query(uri, null,
							Media._ID + "=?", new String[]{Integer.toString(profil.ringtoneId)}, null);
					if (cursor.moveToFirst())
						mTitleRingtone.setText(getString(R.string.recap_title_phone_ringtone) + cursor.getString(cursor.getColumnIndex(Media.TITLE)));
					cursor.close();
				}
				else {
					String name = this.getRingtoneDefaultRingtoneName(RingtoneManager.TYPE_RINGTONE);
					mTitleRingtone.setText(getString(R.string.recap_title_phone_ringtone) + name);
				}
				Integer volume = profil.ringtoneVolume;
				if (volume != null) {
					mVolumeRingtoneSeekBar.setProgress(volume);
				}

				if (profil.notificationId != null && profil.notificationId != ProfilConstant.ID_DEFAULT_ITEM) {
					category = profil.notificationCategory;
					uri = this.getMediaSoundUriByCategory(category);
					cursor = ctx.getContentResolver().query(uri, null,
							Media._ID + "=?", new String[]{Integer.toString(profil.notificationId)}, null);
					if (cursor.moveToFirst())
						mTitleNotification.setText(getString(R.string.recap_title_notification_sound) + cursor.getString(cursor.getColumnIndex(Media.TITLE)));
					cursor.close();
				}
				else {
					String name = this.getRingtoneDefaultRingtoneName(RingtoneManager.TYPE_NOTIFICATION);
					mTitleNotification.setText(getString(R.string.recap_title_notification_sound) + name);
				}
				volume = profil.soundVolume;
				if (volume != null) {
					mVolumeSoundSeekBar.setProgress(volume);
				}

				Boolean value = profil.dialPadTouchTones;
				if (value != null)
					((CheckBox)getView().findViewById(R.id.checkBox_recap_dial_pad_touch_tones)).setChecked(value);
				else
					getView().findViewById(R.id.container_recap_dial_pad_touch_tones).setVisibility(View.GONE);

				value = profil.touchSounds;
				if (value != null)
					((CheckBox)getView().findViewById(R.id.checkBox_recap_touch_sounds)).setChecked(value);
				else
					getView().findViewById(R.id.container_recap_touch_sounds).setVisibility(View.GONE);

				value = profil.screenLockSounds;
				if (value != null)
					((CheckBox)getView().findViewById(R.id.checkBox_recap_screen_lock_sounds)).setChecked(value);
				else
					getView().findViewById(R.id.container_recap_screen_lock_sounds).setVisibility(View.GONE);

				value = profil.vibrateTouch;
				if (value != null)
					((CheckBox)getView().findViewById(R.id.checkBox_recap_vibrate_on_touch)).setChecked(value);
				else
					getView().findViewById(R.id.container_recap_vibrate_on_touch).setVisibility(View.GONE);
			}
			else {
				mIncludeSound.setVisibility(View.GONE);
			}

			// Set param more options
			mCheckWifi.setChecked(profil.wifi);
			mCheckBluethoot.setChecked(profil.bluethoot);

			// Set param background
			if (profil.backgroundId != null && profil.backgroundId != ProfilConstant.ID_DEFAULT_ITEM) {
				mImageLoader.displayImage("file://" + profil.backgroundPath, mImageBackground);
			}
			else {
				mImageBackground.setImageDrawable(WallpaperManager.getInstance(getActivity()).getDrawable());
			}
			mAddress.setText(profil.address);
		}
	}

	@Override
	protected void enterThePage() {
		mActionBar = ((SherlockFragmentActivity)getActivity()).getSupportActionBar();
		mActionBar.setListNavigationCallbacks(null, null);
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

		mListenerObserver.renameNextButton(getString(R.string.recap_name_next_button_last_page));
		mListenerObserver.enableNextButton(false);

		if (mProfilManagement.getValue(Profils.Columns.ID.getName()) != null)
			mIsEditMode = true;

		Integer mode = (Integer)mProfilManagement.getValue(Profils.Columns.MODE.getName());
		if (mode != null && mode == ProfilConstant.TYPE_SONNERIE) {
			this.initRecapRingtone();
		}
		else {
			mIncludeSound.setVisibility(View.GONE);
		}

		if (mActionMode == null) {
			mActionMode = ((SherlockFragmentActivity)getActivity()).startActionMode(new ListenerActionMode());
			mCustomViewActionMode = LayoutInflater.from(getActivity()).inflate(R.layout.view_action_mode_edit_profil, null);
			mActionMode.setCustomView(mCustomViewActionMode);
			
			this.customizeActionModeCloseButton();
		}

		String nameProfil = (String) mProfilManagement.getValue(Profils.Columns.PROFIL_NAME.getName());
		if (nameProfil != null) {
			//			mNameEdit.setText(nameProfil);
			((EditText)mCustomViewActionMode.findViewById(R.id.editText_action_mode_edit_profil)).setText(nameProfil);
		}

		this.initRecapDisplay();
		//		this.initRecapBackground();
		this.initRecapMoreOptions();
		this.initRecapPosition();
	}

	private void customizeActionModeCloseButton() {
		int buttonId = Resources.getSystem().getIdentifier("action_mode_close_button", "id", "android");    
		View v = getActivity().findViewById(buttonId);
		if (v == null)
			buttonId = R.id.abs__action_mode_close_button;
		v = getActivity().findViewById(buttonId);

		if (v == null)
			return;
		LinearLayout ll = (LinearLayout) v;
		if (ll.getChildCount() > 1 && ll.getChildAt(1) != null) {
			TextView tv = (TextView) ll.getChildAt(1);
			tv.setText(getString(R.string.recap_actionmode_close_button));
		}

	}

	@Override
	protected void outOfPage() {
		this.closeActionMode(false);
		mListenerObserver.renameNextButton(getString(R.string.edit_profil_title_button_next));
	}

	@Override
	public void afterTextChanged(Editable s) {
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		if (s.length()  > 0 && mActionMode == null) {
			mActionMode = ((SherlockFragmentActivity)getActivity()).startActionMode(new ListenerActionMode());
			mCustomViewActionMode = LayoutInflater.from(getActivity()).inflate(R.layout.view_action_mode_edit_profil, null);
			mActionMode.setCustomView(mCustomViewActionMode);
		}
		else if (s.length() == 0 && mActionMode != null) {
			mActionMode.finish();
			mActionMode = null;
		}
	}

	private void closeActionMode(Boolean exitAct) {

		if (mActionMode != null) {
			mExitActivity = false;
			mActionMode.finish();
			mActionMode = null;
		}
	}

	class ListenerActionMode implements ActionMode.Callback {

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			menu.add(0, 100, 0, R.string.title_action_mode_validate).setIcon(getResources().getDrawable(R.drawable.navigation_accept_light));
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

			switch (item.getItemId()) {
			case 100:
				String nameProfil = ((EditText)mCustomViewActionMode.findViewById(R.id.editText_action_mode_edit_profil)).getText().toString();
				if (nameProfil != null && TextUtils.isEmpty(nameProfil) == false) {
					mode.finish();
					mode = null;
					mProfilManagement.putStringValue(Profils.Columns.PROFIL_NAME.getName(), nameProfil);
					if (mIsEditMode == false) {
						mProfilManagement
						.putBooleanValue(Profils.Columns.ACTIVE.getName(), true);
						mProfilManagement
						.putBooleanValue(Profils.Columns.IS_SET.getName(), false);
						mProfilManagement.insertProfilValuesInDatabase();

						ManageProfilService.startService(getActivity());

						Toast.makeText(getActivity(), R.string.success_message_create_profil, Toast.LENGTH_SHORT).show();
					}
					else {
						Integer id = (Integer) mProfilManagement.getValue(Profils.Columns.ID.getName());
						mProfilManagement
						.updateProfilValuesInDatabase(Profils.Columns.ID.getName() + "=?", new String[]{Integer.toString(id)});
						Toast.makeText(getActivity(), R.string.success_message_update_profil, Toast.LENGTH_SHORT).show();
					}

					InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(
							Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(((EditText)mCustomViewActionMode
							.findViewById(R.id.editText_action_mode_edit_profil)).getWindowToken(), 0);
					getActivity().finish();
					return true;
				}
				else {
					Toast.makeText(getActivity(), R.string.error_message_missing_name_profil, Toast.LENGTH_SHORT).show();
					return false;
				}

			default:
				break;
			}
			return false;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			if (isAdded()) {
				if (getActivity() != null) {
					InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(
							Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(((EditText)mCustomViewActionMode
							.findViewById(R.id.editText_action_mode_edit_profil)).getWindowToken(), 0);
					mode = null;
					mActionMode = null;

					if (mExitActivity) {
						getActivity().finish();
					}
					else {
						mExitActivity = true;
					}
				}
			}
		}
	}

	private Boolean mExitActivity = true;

	class AsyncLocalization extends AsyncTask<Double, Void, List<Address>> {

		@Override
		protected List<Address> doInBackground(Double... params) {

			Double lat = params[0];
			Double lon = params[1];

			Geocoder geocoder = new Geocoder(getActivity().getApplicationContext(), Locale.getDefault());
			try {
				List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
				if (addresses != null) {
					return addresses;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(List<Address> addresses) {
			super.onPostExecute(addresses);

			String txt;
			if (addresses != null) {
				for (int idx = 0; idx < addresses.size(); ++idx) {
					Address addr = addresses.get(idx);
					mAddress.setText(UtilsGeocoder.getCompleteAddressName(addr));
				}
				txt = mAddress.getText().toString();
			}
			else {
				txt = getResources().getString(R.string.error_message_addresses_not_found);
			}
			mProfilManagement
			.putStringValue(Profils.Columns.ADDRESS.getName(), txt);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		this.closeActionMode(false);
		mTitleRingtone.setTypeface(null);
		mTitleNotification.setTypeface(null);
		mIntensityBrightness.setTypeface(null);
		mAddress.setTypeface(null);
		mProfilManagement = null;
		mIncludeSound = null;
		mTitleRingtone = null;
		mTitleNotification = null;
		mIntensityBrightness = null;
		mAddress = null;
		mImageBackground = null;
	}
}

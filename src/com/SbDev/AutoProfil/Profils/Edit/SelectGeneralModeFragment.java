package com.SbDev.AutoProfil.Profils.Edit;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.Profils.Model.PageFragment;
import com.SbDev.AutoProfil.data.Local.DataInterface.ProfilConstant;
import com.SbDev.AutoProfil.data.Local.ProfilManagement;
import com.SbDev.AutoProfil.data.provider.AutoProfilContent.Profils;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class SelectGeneralModeFragment extends PageFragment implements OnClickListener {

	private ActionBar						mActionBar;
	
	private RadioGroup						mRadioGroup;
	private int 							mBranchSelected = 0;

	private ProfilManagement				mProfilManagement;
	private Boolean							mIsEditMode = false;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_general_mode, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mProfilManagement = ProfilManagement.getInstance(getActivity().getApplicationContext());
		
		if (mProfilManagement.getValue(Profils.Columns.ID.getName()) != null)
			mIsEditMode = true;
		
		if (mIsEditMode == true) {
			if ((Integer)mProfilManagement.getValue(Profils.Columns.MODE.getName()) == ProfilConstant.TYPE_SONNERIE) {
				((RadioButton)view.findViewById(R.id.radio_sonnerie)).setChecked(true);
				callbackSelectBranch(0);
			}
			else if ((Integer)mProfilManagement.getValue(Profils.Columns.MODE.getName()) == ProfilConstant.TYPE_VIBREUR) {
				((RadioButton)view.findViewById(R.id.radio_vibrate)).setChecked(true);
				callbackSelectBranch(1);
			}
			else if ((Integer)mProfilManagement.getValue(Profils.Columns.MODE.getName()) == ProfilConstant.TYPE_SILENCE) {
				((RadioButton)view.findViewById(R.id.radio_silence)).setChecked(true);
				callbackSelectBranch(2);
			}
		}
		else
			mProfilManagement.putIntegerValue(Profils.Columns.MODE.getName(), ProfilConstant.TYPE_SONNERIE);

		mRadioGroup = (RadioGroup) view.findViewById(R.id.radioGroup_general_mode);
		for (int idx = 0; idx < mRadioGroup.getChildCount(); ++idx)
			mRadioGroup.getChildAt(idx).setOnClickListener(this);
	}
	
	@Override
	protected void enterThePage() {
		if (((SherlockFragmentActivity)getActivity()) != null) {
			mActionBar = ((SherlockFragmentActivity)getActivity()).getSupportActionBar();
			mActionBar.setListNavigationCallbacks(null, null);
			mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		}

		mListenerObserver.enableNextButton(true);
	}
	
	@Override
	protected void outOfPage() {
	}

	@Override
	public void onClick(View v) {

		if (mIsEditMode == false)
			mProfilManagement.clearValues();
		
		if (mRadioGroup.getCheckedRadioButtonId() == R.id.radio_sonnerie) {
			mProfilManagement.putIntegerValue(Profils.Columns.MODE.getName(), ProfilConstant.TYPE_SONNERIE);
			callbackSelectBranch(0);
		}
		else if (mRadioGroup.getCheckedRadioButtonId() == R.id.radio_vibrate) {
			mProfilManagement.putIntegerValue(Profils.Columns.MODE.getName(), ProfilConstant.TYPE_VIBREUR);
			callbackSelectBranch(1);
		}
		else if (mRadioGroup.getCheckedRadioButtonId() == R.id.radio_silence) {
			mProfilManagement.putIntegerValue(Profils.Columns.MODE.getName(), ProfilConstant.TYPE_SILENCE);
			callbackSelectBranch(2);
		}
	}
}

package com.SbDev.AutoProfil.Profils.Edit;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.Base.BaseMapFragment;
import com.SbDev.AutoProfil.Dialog.DialogSearchAddress;
import com.SbDev.AutoProfil.data.Local.DataInterface.PreferenceFields;
import com.SbDev.AutoProfil.data.Local.ProfilManagement;
import com.SbDev.AutoProfil.data.provider.AutoProfilContent.Profils;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import com.bonadevapps.compoments.preferences.PreferenceManager;
import com.bonadevapps.utils.UtilsGeocoder;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class LocalizationProfilMapFragment extends BaseMapFragment implements OnMapClickListener, OnQueryTextListener, OnAddressSelected, 
OnMarkerDragListener {

	public static final String					TAG_MAP_FRAGMENT = "mapProfilFragment";

	private static final float					ZOOM_LEVEL_ADD_MARKER = 17f;
	
	private Marker								mMarkerProfil;
	private Circle								mCircle;
	private OnMarkerAdded						mListenerMarkerAdded;

	public LocalizationProfilMapFragment() {
		super();
	}

	public static LocalizationProfilMapFragment newInstance() {

		LocalizationProfilMapFragment map = new LocalizationProfilMapFragment();
		return map;
	}

	public static LocalizationProfilMapFragment newInstance(OnMarkerAdded listener) {

		LocalizationProfilMapFragment map = new LocalizationProfilMapFragment();
		map.initListenerAddMarker(listener);
		return map;
	}

	public void initListenerAddMarker(OnMarkerAdded listener) {
		mListenerMarkerAdded = listener;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if (mMap != null) {
			mMap.setOnMapClickListener(this);
			mMap.setOnMarkerDragListener(this);
			
			if (getActivity() instanceof EditProfilActivity) {
				SearchView searchView = ((EditProfilActivity)getActivity()).getSearchView();
				searchView.setOnQueryTextListener(this);
			}
		}
	}

	public void initCameraPosition() {
		if (ProfilManagement.getInstance(getActivity()).getValue(Profils.Columns.LATITUDE.getName()) == null &&
				ProfilManagement.getInstance(getActivity()).getValue(Profils.Columns.LONGITUDE.getName()) == null) {
			// No existing marker and center on user position
			centerOnMyLocation();
		}
		else {
			// Center map on existing marker
			String lat = (String) ProfilManagement.getInstance(getActivity()).getValue(Profils.Columns.LATITUDE.getName());
			String lon = (String) ProfilManagement.getInstance(getActivity()).getValue(Profils.Columns.LONGITUDE.getName());
			animateCameraWithProp(new LatLng(Double.valueOf(lat), Double.valueOf(lon)), ZOOM_LEVEL_ADD_MARKER, null, null);
		}
	}

	@Override
	public void onMapClick(LatLng point) {
		mCancelCenterUser = true;
		ProfilManagement.getInstance(getActivity()).removeValue(Profils.Columns.ADDRESS.getName());
		this.addMarker(point);
		this.drawCircle(point);
	}

	private void addMarker(LatLng point) {
		ProfilManagement.getInstance(getActivity().getApplicationContext()).
		putStringValue(Profils.Columns.LATITUDE.getName(), Double.toString(point.latitude));
		ProfilManagement.getInstance(getActivity().getApplicationContext()).
		putStringValue(Profils.Columns.LONGITUDE.getName(), Double.toString(point.longitude));

		float zoom = mMap.getCameraPosition().zoom;
		if (ZOOM_LEVEL_ADD_MARKER > mMap.getCameraPosition().zoom) {
			zoom = ZOOM_LEVEL_ADD_MARKER;
		}
		animateCameraWithProp(point, zoom, null, null);
		
		mMap.clear();

		if (mMarkerProfil != null) {
			mMarkerProfil.remove();
		}
		mMarkerProfil = mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)).position(point));
		
		mMarkerProfil.setDraggable(true);
		
		mListenerMarkerAdded.onMarkerAddedOnMap(mMarkerProfil);
	}

	private void drawCircle(LatLng point) {
		
		int radiusValue = PreferenceManager.getInstance(getActivity()).getInt(PreferenceFields.RADIUS_PROFILE_RESEARCH);
		Double radius = Double.valueOf(radiusValue);
		CircleOptions circleOption = createCircle(point, R.color.holo_green_light, R.color.holo_green_light_trans, radius);
		
		if (mCircle != null) {
			mCircle.remove();
		}
		
		mCircle = mMap.addCircle(circleOption);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (getActivity() instanceof EditProfilActivity) {
			((EditProfilActivity)getActivity()).getSearchView().setOnQueryTextListener(null);
		}
		if (mMap != null) {
			mMap.setOnMapClickListener(null);
			mMap.setOnMarkerDragListener(null);
		}
	}

	public void putExistingMarker(LatLng position) {
		if (mMap != null) {
			mMarkerProfil = mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)).position(position));
			mMarkerProfil.setDraggable(true);
			
			int radiusValue = PreferenceManager.getInstance(getActivity()).getInt(PreferenceFields.RADIUS_PROFILE_RESEARCH);
			Double radius = Double.valueOf(radiusValue);
			CircleOptions circleOption = createCircle(position, R.color.holo_green_light, R.color.holo_green_light_trans, radius);
			mCircle = mMap.addCircle(circleOption);
		}
	}

	@Override
	protected void preloadImageOnCustomMarker(Marker marker) {
	}

	@SuppressLint("NewApi")
	@Override
	public boolean onQueryTextSubmit(String query) {
		AsyncTaskGeocoderAdress async = new AsyncTaskGeocoderAdress();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, query);
		} else {
			async.execute(query);
		}
		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		return false;
	}

	@Override
	public void addressSelected(Address address) {
		LatLng pos = new LatLng(address.getLatitude(), address.getLongitude());
		String addr = UtilsGeocoder.getCompleteAddressName(address);
		ProfilManagement.getInstance(getActivity()).putStringValue(Profils.Columns.ADDRESS.getName(), addr);
		if (getActivity() instanceof EditProfilActivity) {
			((EditProfilActivity)getActivity()).collapseActionViewItemSearch();
		}
		this.addMarker(pos);
	}

	@Override
	public void onMarkerDrag(Marker marker) {
		mCircle.setCenter(marker.getPosition());
	}

	@Override
	public void onMarkerDragEnd(Marker marker) {
		mCircle.setCenter(marker.getPosition());
		mCircle.setFillColor(getResources().getColor(R.color.holo_green_light_trans));
	}

	@Override
	public void onMarkerDragStart(Marker marker) {
		mCircle.setCenter(marker.getPosition());
		mCircle.setFillColor(Color.TRANSPARENT);
	}
	
	private class AsyncTaskGeocoderAdress extends AsyncTask<String, Void, List<Address>> {

		private static final int			MAX_RESULT_ADDRESS = 10;

		@Override
		protected List<Address> doInBackground(String... params) {

			Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
			try {
				List<Address> list = geocoder.getFromLocationName(params[0], MAX_RESULT_ADDRESS);
				return list;
			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(List<Address> result) {
			super.onPostExecute(result);
			if (result != null) {
				if (result.size() > 1) {
					DialogSearchAddress.show((SherlockFragmentActivity)getActivity(), getString(R.string.title_dialog_address), result, LocalizationProfilMapFragment.this);
				}
				else {
					LatLng point = new LatLng(result.get(0).getLatitude(), result.get(0).getLongitude());
					String address = UtilsGeocoder.getCompleteAddressName(result.get(0));
					ProfilManagement.getInstance(getActivity()).putStringValue(Profils.Columns.ADDRESS.getName(), address);
					if (getActivity() instanceof EditProfilActivity) {
						((EditProfilActivity)getActivity()).collapseActionViewItemSearch();
					}
					addMarker(point);
				}
			}
		}
	}

	@Override
	public void onCameraChange(CameraPosition position) {
		mCancelCenterUser = true;
	}
}
package com.SbDev.AutoProfil.Profils.Edit;

import java.util.HashMap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.Profils.Model.PageFragment;
import com.SbDev.AutoProfil.data.Local.ProfilManagement;
import com.SbDev.AutoProfil.data.provider.AutoProfilContent.Profils;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.bonadevapps.compoments.checkableLayouts.CheckableLinearLayout;

public class SelectConnectivityParamFragment extends PageFragment implements OnItemClickListener {

	private ActionBar							mActionBar;

	private ListView							mListOptions;

	private OptionsAdapter						mAdapter;

	private HashMap<String, String>				mOptions;
	private String[]							mTabOptions;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_select_connectivity_param, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mListOptions = (ListView) view.findViewById(R.id.listView_more_options);
		mListOptions.setOnItemClickListener(this);
		mListOptions.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

		mTabOptions = getResources().getStringArray(R.array.array_list_more_options);

		mAdapter = new OptionsAdapter();
		mListOptions.setAdapter(mAdapter);

		mOptions = new HashMap<String, String>();
		mOptions.put(mTabOptions[0], Profils.Columns.WIFI.getName());
		mOptions.put(mTabOptions[1], Profils.Columns.BLUETHOOT.getName());
		
//		mOptions.put(mTabOptions[1], Profils.Columns.WIFI_DIRECT.getName());
//		mOptions.put(mTabOptions[3], Profils.Columns.NFC.getName());
//		mOptions.put(mTabOptions[4], Profils.Columns.DEBUG_MODE.getName());
	}

	@Override
	protected void enterThePage() {

		mActionBar = ((SherlockFragmentActivity)getActivity()).getSupportActionBar();
		mActionBar.setListNavigationCallbacks(null, null);
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		mListenerObserver.enableNextButton(true);
	}

	@Override
	protected void outOfPage() {
	}

	@Override
	public void onItemClick(AdapterView<?> adp, View view, int position, long id) {
		
		ViewHolderOptions holder = (ViewHolderOptions) view.getTag();
		holder.mCheck.setChecked(((CheckableLinearLayout)view).isChecked());
		ProfilManagement.getInstance(getActivity().getApplicationContext())
		.putBooleanValue(mOptions.get(mTabOptions[position]), holder.mCheck.isChecked());
	}

	public class ViewHolderOptions {

		public TextView mName;
		public CheckBox mCheck;

		public ViewHolderOptions(View v) {

			mName = (TextView) v.findViewById(R.id.textView_name_option);
			mCheck = (CheckBox) v.findViewById(R.id.checkBox_select_option);
		}
	}

	public class OptionsAdapter extends BaseAdapter {

		ProfilManagement pm = ProfilManagement.getInstance(getActivity().getApplicationContext());

		@Override
		public int getCount() {
			return mTabOptions.length;
		}

		@Override
		public Object getItem(int position) {
			return mTabOptions[position];
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			ViewHolderOptions holder = null;
			if (convertView == null) {
				convertView = LayoutInflater.from(getActivity()).inflate(R.layout.cell_options_list, null);
				holder = new ViewHolderOptions(convertView);
				convertView.setTag(holder);
			}
			else {
				holder = (ViewHolderOptions) convertView.getTag();
			}

			holder.mName.setText(mTabOptions[position]);
//			holder.mCheck.setOnClickListener(new OnClickListener() {
//				@Override
//				public void onClick(View v) {
//					pm.putBooleanValue(mOptions.get(mTabOptions[position]), ((CheckBox)v).isChecked());
//				}
//			});

			Boolean value = (Boolean) pm.getValue(mOptions.get(mTabOptions[position]));
			if (value == null || (Boolean)value == false) {
				holder.mCheck.setChecked(false);
			}
			else {
				holder.mCheck.setChecked(true);
			}
			return convertView;
		}

	}
}

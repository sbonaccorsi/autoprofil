package com.SbDev.AutoProfil.Profils.Edit;

import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.Profils.Model.PageFragment;
import com.SbDev.AutoProfil.data.Local.ProfilManagement;
import com.SbDev.AutoProfil.data.provider.AutoProfilContent.Profils;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class SelectSoundParamFragment extends PageFragment implements OnSeekBarChangeListener, OnClickListener {

	private ActionBar				mActionBar;

	private ProfilManagement		mProfilManagement;
	private AudioManager			mAudioManager;
	private int						mCurrentVolumeRingtone;
	private int						mCurrentVolumeSound;

	private CheckBox				mDialPad;
	private CheckBox				mTouchSounds;
	private CheckBox				mScreenLock;
	private CheckBox				mVibrateTouch;

	private SeekBar					mControlVolumeRingtone;
	private SeekBar					mControlVolumeSound;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_select_other_sound_params, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mProfilManagement = ProfilManagement.getInstance(getActivity());

		mDialPad = (CheckBox) view.findViewById(R.id.checkBox_dial_pad_touch_tones);
		mTouchSounds = (CheckBox) view.findViewById(R.id.checkBox_touch_sounds);
		mScreenLock = (CheckBox) view.findViewById(R.id.checkBox_screen_lock_sound);
		mVibrateTouch = (CheckBox) view.findViewById(R.id.checkBox_vibrate_on_touch);

		mDialPad.setOnClickListener(this);
		mTouchSounds.setOnClickListener(this);
		mScreenLock.setOnClickListener(this);
		mVibrateTouch.setOnClickListener(this);

		mAudioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);

		mControlVolumeRingtone = (SeekBar) view.findViewById(R.id.seekBar_control_volume_ringtone);
		mControlVolumeSound = (SeekBar) view.findViewById(R.id.seekBar_control_volume_sound);

		// Handle touch sound
		//		System.putInt(getActivity().getContentResolver(), System.SOUND_EFFECTS_ENABLED, 1);

		// Handle dial pad tone
		//		System.putInt(getActivity().getContentResolver(), System.DTMF_TONE_WHEN_DIALING, 1);

		// Handle vibrate on touch
		//		System.putInt(getActivity().getContentResolver(), System.HAPTIC_FEEDBACK_ENABLED, 0);

		// Handle lockscreen sound
		//		System.putInt(getActivity().getContentResolver(), "lockscreen_sounds_enabled", 1);
		//		Global.putInt(getActivity().getContentResolver(), Global.ADB_ENABLED, 0);

	}

	public void restorePreviousConfiguration() {
		mAudioManager.setStreamVolume(AudioManager.STREAM_RING, mCurrentVolumeRingtone, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
		mControlVolumeRingtone.setOnSeekBarChangeListener(null);
		mControlVolumeSound.setOnSeekBarChangeListener(null);
	}

	@Override
	protected void enterThePage() {
		mActionBar = ((SherlockFragmentActivity)getActivity()).getSupportActionBar();
		mActionBar.setListNavigationCallbacks(null, null);
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

		int maxVolumeRingtone = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
		int maxVolumeSound = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

		mCurrentVolumeRingtone = mAudioManager.getStreamVolume(AudioManager.STREAM_RING);
		mCurrentVolumeSound = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

		mControlVolumeRingtone.setMax(maxVolumeRingtone);
		mControlVolumeSound.setMax(maxVolumeSound);

		Integer volumeRingtone = (Integer) mProfilManagement.getValue(Profils.Columns.NOTIFICATION_RINGTONE_LEVEL.getName());
		if (volumeRingtone != null && volumeRingtone != 0) {
			mControlVolumeRingtone.setProgress(volumeRingtone);
			mProfilManagement.putIntegerValue(Profils.Columns.NOTIFICATION_RINGTONE_LEVEL.getName(), volumeRingtone);
		}
		else {
			if (mCurrentVolumeRingtone == 0) {
				mAudioManager.setStreamVolume(AudioManager.STREAM_RING, mCurrentVolumeRingtone + 1, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
				mControlVolumeRingtone.setProgress(mCurrentVolumeRingtone + 1);	
			}
			else {
				mControlVolumeRingtone.setProgress(mCurrentVolumeRingtone);
			}
			mProfilManagement.putIntegerValue(Profils.Columns.NOTIFICATION_RINGTONE_LEVEL.getName(), mCurrentVolumeRingtone);
		}

		Integer volumeSound = (Integer) mProfilManagement.getValue(Profils.Columns.SOUND_LEVEL.getName());
		if (volumeSound != null) {
			mControlVolumeSound.setProgress(volumeSound);
			mProfilManagement.putIntegerValue(Profils.Columns.SOUND_LEVEL.getName(), volumeSound);
		}
		else {
			mControlVolumeSound.setProgress(mCurrentVolumeSound);
			mProfilManagement.putIntegerValue(Profils.Columns.SOUND_LEVEL.getName(), mCurrentVolumeSound);
		}

		this.initDialPadTones();
		this.initTouchSounds();
		this.initScreenLockSounds();
		this.initVibrateTouch();

		mControlVolumeRingtone.setOnSeekBarChangeListener(this);
		mControlVolumeSound.setOnSeekBarChangeListener(this);
	}

	private void initDialPadTones() {
		Boolean value = null;
		try {
			System.getInt(getActivity().getContentResolver(), System.DTMF_TONE_WHEN_DIALING);
			if (mProfilManagement.getValue(Profils.Columns.DIAL_PAD_TONES.getName()) != null) {
				value = (Boolean) mProfilManagement.getValue(Profils.Columns.DIAL_PAD_TONES.getName());
				mDialPad.setChecked(value);
			}
			else {
				//			System.putInt(getActivity().getContentResolver(), System.DTMF_TONE_WHEN_DIALING, 1);
				value = System.getInt(getActivity().getContentResolver(), System.DTMF_TONE_WHEN_DIALING) != 0 ? true : false;
				mProfilManagement.putBooleanValue(Profils.Columns.DIAL_PAD_TONES.getName(), value);
				mDialPad.setChecked(value);

			}
		}
		catch (SettingNotFoundException e) {
			if (getView() != null) {
				getView().findViewById(R.id.container_dial_pad_touch_tones).setVisibility(View.GONE);
			}
		}
	}

	private void initTouchSounds() {
		Boolean value = null;
		try {
			System.getInt(getActivity().getContentResolver(), System.SOUND_EFFECTS_ENABLED);
			if (mProfilManagement.getValue(Profils.Columns.TOUCH_SOUNDS.getName()) != null) {
				value = (Boolean) mProfilManagement.getValue(Profils.Columns.TOUCH_SOUNDS.getName());
				mTouchSounds.setChecked(value);
			}
			else {
				//				System.putInt(getActivity().getContentResolver(), System.SOUND_EFFECTS_ENABLED, 1);

				value = System.getInt(getActivity().getContentResolver(), System.SOUND_EFFECTS_ENABLED) != 0 ? true : false;
				mProfilManagement.putBooleanValue(Profils.Columns.TOUCH_SOUNDS.getName(), value);
				mTouchSounds.setChecked(value);
			}
		}
		catch (SettingNotFoundException e) {
			if (getView() != null) {
				getView().findViewById(R.id.container_touch_sounds).setVisibility(View.GONE);
			}
		}
	}

	private void initScreenLockSounds() {
		Boolean value = null;
		try {
			System.getInt(getActivity().getContentResolver(), "lockscreen_sounds_enabled");
			if (mProfilManagement.getValue(Profils.Columns.SCREEN_LOCK_SOUNDS.getName()) != null) {
				value = (Boolean) mProfilManagement.getValue(Profils.Columns.SCREEN_LOCK_SOUNDS.getName());
				mScreenLock.setChecked(value);
			}
			else {
				//				System.putInt(getActivity().getContentResolver(), "lockscreen_sounds_enabled", 1);

				value = System.getInt(getActivity().getContentResolver(), "lockscreen_sounds_enabled") != 0 ? true : false;
				mProfilManagement.putBooleanValue(Profils.Columns.SCREEN_LOCK_SOUNDS.getName(), value);
				mScreenLock.setChecked(value);
			}
		}
		catch (SettingNotFoundException e) {
			if (getView() != null) {
				getView().findViewById(R.id.container_screen_lock_sound).setVisibility(View.GONE);
			}
		}
	}

	private void initVibrateTouch() {
		Boolean value = null;
		try {
			System.getInt(getActivity().getContentResolver(), System.HAPTIC_FEEDBACK_ENABLED);
			if (mProfilManagement.getValue(Profils.Columns.VIBRATE_TOUCH.getName()) != null) {
				value = (Boolean) mProfilManagement.getValue(Profils.Columns.VIBRATE_TOUCH.getName());
				mVibrateTouch.setChecked(value);
			}
			else {
				//				System.putInt(getActivity().getContentResolver(), System.HAPTIC_FEEDBACK_ENABLED, 0);
				value = System.getInt(getActivity().getContentResolver(), System.HAPTIC_FEEDBACK_ENABLED) != 0 ? true : false;
				mProfilManagement.putBooleanValue(Profils.Columns.VIBRATE_TOUCH.getName(), value);
				mVibrateTouch.setChecked(value);
			}
		}
		catch (SettingNotFoundException e) {
			if (getView() != null) {
				getView().findViewById(R.id.container_vibrate_on_touch).setVisibility(View.GONE);
			}
		}
	}

	@Override
	protected void outOfPage() {
		this.restorePreviousConfiguration();
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.checkBox_dial_pad_touch_tones:
			mProfilManagement.putBooleanValue(Profils.Columns.DIAL_PAD_TONES.getName(), ((CheckBox)v).isChecked());
			break;

		case R.id.checkBox_touch_sounds:
			mProfilManagement.putBooleanValue(Profils.Columns.TOUCH_SOUNDS.getName(), ((CheckBox)v).isChecked());
			break;

		case R.id.checkBox_screen_lock_sound:
			mProfilManagement.putBooleanValue(Profils.Columns.SCREEN_LOCK_SOUNDS.getName(), ((CheckBox)v).isChecked());
			break;

		case R.id.checkBox_vibrate_on_touch:
			mProfilManagement.putBooleanValue(Profils.Columns.VIBRATE_TOUCH.getName(), ((CheckBox)v).isChecked());
			break;

		default:
			break;
		}
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

		switch (seekBar.getId()) {
		case R.id.seekBar_control_volume_ringtone:
			if (progress == 0) {
				progress = 1;
				seekBar.setProgress(progress);
			}

			if (fromUser) {
				mAudioManager.setStreamVolume(AudioManager.STREAM_RING, progress, AudioManager.FLAG_PLAY_SOUND);
			}
			else {
				mAudioManager.setStreamVolume(AudioManager.STREAM_RING, progress, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
			}
			mProfilManagement.putIntegerValue(Profils.Columns.NOTIFICATION_RINGTONE_LEVEL.getName(), progress);
			break;

		case R.id.seekBar_control_volume_sound :
			mProfilManagement.putIntegerValue(Profils.Columns.SOUND_LEVEL.getName(), progress);
			break;

		default:
			break;
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar arg0) {
	}

	@Override
	public void onStopTrackingTouch(SeekBar arg0) {
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		this.restorePreviousConfiguration();
	}
}

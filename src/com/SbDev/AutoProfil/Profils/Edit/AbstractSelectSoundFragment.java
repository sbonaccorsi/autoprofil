package com.SbDev.AutoProfil.Profils.Edit;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Audio.Media;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.Profils.Model.PageFragment;
import com.SbDev.AutoProfil.data.Local.DataInterface.IdCursorLoader;
import com.SbDev.AutoProfil.data.Local.DataInterface.ProfilConstant;
import com.SbDev.AutoProfil.data.Local.ProfilManagement;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.bonadevapps.compoments.checkableLayouts.CheckableLinearLayout;
import com.bonadevapps.compoments.noFocus.ImageButtonNoFocusable;
import com.bonadevapps.compoments.soundplayer.PlayerListener;
import com.bonadevapps.compoments.soundplayer.SoundPlayer;

public abstract class AbstractSelectSoundFragment extends PageFragment implements LoaderCallbacks<Cursor>, 
OnItemClickListener {

	protected Uri							mUriMedia;

	protected String						mMedia_Cat1;
	protected String						mMedia_Cat2;

	protected String						SOUND_CATEGORY;
	protected String						SOUND_ID;
	protected String						SOUND_DATA;

	protected List<String> 					mTabNavigation;

	private ActionBar						mActionBar;

	protected ListRingtoneAdapter 			mAdapter;

	private ProfilManagement				mProfilManagment;

	private SoundPlayer						mSoundPlayer;

	protected TextView						mTitleRingtone;
	private ListView						mListView;
	private View							mHeader;

	protected String						mCategory;
	private boolean 						mHandleNav = false;

	private List<Long> 						mListChecked = new ArrayList<Long>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_select_sound, container, false);
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mSoundPlayer = new SoundPlayer(getActivity().getBaseContext());
		mSoundPlayer.setIsPreviewSound(10d, true);

		mProfilManagment = ProfilManagement.getInstance(getActivity().getApplicationContext());

		mTitleRingtone = (TextView) view.findViewById(R.id.textView_title_ringtone);

		mListView = (ListView) view.findViewById(R.id.list_ringtone);
		mListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
		mListView.setFastScrollEnabled(true);

		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mHeader = inflater.inflate(R.layout.header_keep_actual, null);
		((TextView)mHeader.findViewById(R.id.textView_header_keep_actual)).setText("Keep actual ringtone");

		mListView.addHeaderView(mHeader);

		Integer soundId = (Integer) mProfilManagment.getValue(SOUND_ID);
		if (soundId != null && soundId != 0) {
			mListChecked.add(Long.valueOf(soundId.longValue()));
		}

		mAdapter = new ListRingtoneAdapter(getActivity(), R.layout.cell_ringtone_select, null, 
				new String[]{}, new int[]{}, 0);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(this);
		mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		mCategory = mMedia_Cat1;
	}

	private void initActionBarState() {
		mActionBar = ((SherlockFragmentActivity)getActivity()).getSupportActionBar();
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

		ArrayAdapter<String> adp = new ArrayAdapter<String>(mActionBar.getThemedContext(), R.layout.custom_spinner_item, mTabNavigation);
		adp.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);

		mActionBar.setListNavigationCallbacks(adp, new OnNavigationListener() {

			@Override
			public boolean onNavigationItemSelected(int itemPosition, long itemId) {
				if (mHandleNav == true) {
					String type = null;
					if (itemPosition == 0) {
						mUriMedia = Media.EXTERNAL_CONTENT_URI;
						type = mMedia_Cat1;
					}
					else if (itemPosition == 1) {
						mUriMedia = Media.INTERNAL_CONTENT_URI;
						type = mMedia_Cat2;
					}
					updateCategorySelected(type);
					handleNextButton();
				}
				return true;
			}
		});
		if (mCategory.contentEquals(mMedia_Cat1)) {
			mHandleNav = true;
			mActionBar.setSelectedNavigationItem(0);
		}
		else if (mCategory.contentEquals(mMedia_Cat2)) {
			mHandleNav = true;
			mActionBar.setSelectedNavigationItem(1);
		}
	}

	protected void updateCategorySelected(String category) {
		mCategory = category;
		mAdapter.notifyDataSetChanged();
	}

	@Override
	protected void enterThePage() {

		Integer category = (Integer) mProfilManagment.getValue(SOUND_CATEGORY);
		if (category == null) {
			mCategory = mMedia_Cat1;
		}
		else {
			
			if (category == ProfilConstant.CATEGORY_MUSIC) {
				mCategory = mMedia_Cat1;
			}
			else if (category == ProfilConstant.CATEGORY_RINGTONE) {
				mCategory = mMedia_Cat2;
			}
		}

		this.initActionBarState();
	}

	private void handleNextButton() {
		Integer soundId = (Integer) mProfilManagment.getValue(SOUND_ID);
		if (mIsForced && soundId == null || soundId == 0) {
			mListenerObserver.enableNextButton(false);
		}
		else if (mProfilManagment.getValue(SOUND_ID) != null && soundId != 0) {
			Integer id = (Integer) mProfilManagment.getValue(SOUND_ID);
			if (id == ProfilConstant.ID_DEFAULT_ITEM) {
				((CheckBox)mHeader.findViewById(R.id.checkBox_header_keep_actual)).setChecked(true);
			}
			mListChecked.add(Long.valueOf(id));
			mListenerObserver.enableNextButton(true);
		}
	}
	
	@Override
	protected void outOfPage() {
		mSoundPlayer.stopSound();
		mHandleNav = false;
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {

		switch (loader.getId()) {
		case IdCursorLoader.ID_CURSOR_RINGTONE:
		case IdCursorLoader.ID_CURSOR_NOTIFICATION:
			mAdapter.changeCursor(null);
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> adp, View view, int position, long id) {

		if (view == mHeader) {
			CheckBox checkHeader = (CheckBox) mHeader.findViewById(R.id.checkBox_header_keep_actual);
			if (checkHeader.isChecked() == false) {
				checkHeader.setChecked(true);
				callbackStateNextButton(true);
				mListChecked.clear();
				mListChecked.add(Long.valueOf(ProfilConstant.ID_DEFAULT_ITEM));
				mAdapter.notifyDataSetChanged();
				mProfilManagment.putIntegerValue(SOUND_ID, ProfilConstant.ID_DEFAULT_ITEM);
				mProfilManagment.removeValue(SOUND_DATA);
			}
			else {
				mProfilManagment.removeValue(SOUND_ID);
				mProfilManagment.removeValue(SOUND_DATA);
				mListChecked.clear();
				mAdapter.notifyDataSetChanged();
				checkHeader.setChecked(false);
				callbackStateNextButton(false);
			}
		}
		else {
			((CheckBox) mHeader.findViewById(R.id.checkBox_header_keep_actual)).setChecked(false);
			ViewHolderRingtone holder = (ViewHolderRingtone) view.getTag();
			Boolean isChecked = ((CheckableLinearLayout)view).isChecked();

			if (mListChecked != null && mListChecked.size() != 0) {
				if (mListChecked.get(0).longValue() == holder.mID.longValue()) {
					isChecked = false;
				}
			}
			holder.mCheckRingtone.setChecked(isChecked);
			mListChecked.clear();
			mSoundPlayer.stopSound();
			if (isChecked == true) {

				mProfilManagment
				.putIntegerValue(SOUND_ID, holder.mID.intValue());
				mProfilManagment
				.putStringValue(SOUND_DATA, holder.mData);

				if (mCategory.contentEquals(mMedia_Cat1)) {
					mProfilManagment
					.putIntegerValue(SOUND_CATEGORY, ProfilConstant.CATEGORY_MUSIC);
				}
				else if (mCategory.contentEquals(mMedia_Cat2)) {
					mProfilManagment
					.putIntegerValue(SOUND_CATEGORY, ProfilConstant.CATEGORY_RINGTONE);
				}

				mListChecked.add(holder.mID);
			}
			else {
				mProfilManagment
				.removeValue(SOUND_ID);
				mProfilManagment
				.removeValue(SOUND_DATA);
			}
			callbackStateNextButton(isChecked);
			mAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mSoundPlayer.releasePlayer();
		mSoundPlayer.setPlayerListener(null);
		mSoundPlayer = null;
		mListView.setAdapter(null);
		//		getActivity().getSupportLoaderManager().destroyLoader(IdCursorLoader.ID_CURSOR_RINGTONE);
	}

	private void manageSound(final ViewHolderRingtone holder) {
		mSoundPlayer.setPlayerListener(new PlayerListener() {
			@Override
			public void stopSound() {
				holder.mPlayerButton.toggle();
			}
			@Override
			public void startSound() {
				holder.mPlayerButton.toggle();
			}
			@Override
			public void loadedSound() {
			}
		});

		if (holder.mPlayerButton.isChecked()) {
			mSoundPlayer.stopSound();
		}
		else {
			mSoundPlayer.loadSoundByPath(holder.mData);
			mSoundPlayer.playSound();
		}
	}

	public void onClickView(View cell, View view, int positionViewAdp) {

		ViewHolderRingtone holder = (ViewHolderRingtone) cell.getTag();

		switch (view.getId()) {
		case R.id.imageButton_player_control:
			this.manageSound(holder);
			break;

		default:
			break;
		}
	}

	//#######################
	// Adapter list ringtone

	class ViewHolderRingtone {

		public LinearLayout mLayoutCell;
		public TextView mNameRingtone;
		public TextView mNameArtist;
		public CheckBox mCheckRingtone;
		public LinearLayout mVolumeControl;
		public ImageButtonNoFocusable mPlayerButton;

		public Long mID;
		public String mData;

		public ViewHolderRingtone(View v) {

			mLayoutCell = (LinearLayout) v.findViewById(R.id.layout_cell_song);
			mNameRingtone = (TextView) v.findViewById(R.id.textView_name_song);
			mNameArtist = (TextView) v.findViewById(R.id.textView_artist_song);
			mCheckRingtone = (CheckBox) v.findViewById(R.id.checkBox_select_song);
			mVolumeControl = (LinearLayout) v.findViewById(R.id.container_preview_ring);
			mPlayerButton = (ImageButtonNoFocusable) v.findViewById(R.id.imageButton_player_control);
		}

		public void setOnClickListener(final View cell, final int positionViewAdp) {
			mPlayerButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					onClickView(cell, v, positionViewAdp);
				}
			});
		}
	}

	class ListRingtoneAdapter extends SimpleCursorAdapter {

		public ListRingtoneAdapter(Context context, int layout, Cursor c,
				String[] from, int[] to, int flags) {
			super(context, layout, c, from, to, flags);
		}

		@Override
		public Cursor getCursor() {
			return super.getCursor();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup viewGroup) {

			final View view = super.getView(position, convertView, viewGroup);
			final Cursor cursor = getCursor();

			final ViewHolderRingtone holder;
			if (view.getTag() == null) {
				ViewHolderRingtone h = new ViewHolderRingtone(view);
				holder = h;
				view.setTag(holder);
			}
			else
				holder = (ViewHolderRingtone) view.getTag();

			if (cursor.moveToPosition(position)) {

				final int indexColID = cursor.getColumnIndex(Media._ID);
				final int indexColDATA = cursor.getColumnIndex(Media.DATA);

				holder.mNameRingtone.setText(cursor.getString(cursor.getColumnIndex(Media.TITLE)));
				holder.mNameArtist.setText(cursor.getString(cursor.getColumnIndex(Media.ARTIST)));
				holder.mID = Long.valueOf(cursor.getLong(indexColID));
				holder.mData = cursor.getString(indexColDATA);

				holder.setOnClickListener(view, position);

				if (mListChecked.size() != 0) {
					if (mListChecked.get(0).longValue() == holder.mID.longValue()) {
						holder.mCheckRingtone.setChecked(true);
						holder.mVolumeControl.setVisibility(View.VISIBLE);

						if (mSoundPlayer.getPathLoaded() != null && holder.mData.contentEquals(mSoundPlayer.getPathLoaded())) {
							if (mSoundPlayer.isPlaying()) {
								holder.mPlayerButton.setChecked(true);
								holder.mPlayerButton.setImageResource(R.drawable.av_stop_light);
							}
							else {
								holder.mPlayerButton.setChecked(false);
								holder.mPlayerButton.setImageResource(R.drawable.av_play_light);
							}
						}
						else {
							holder.mPlayerButton.setChecked(false);
							holder.mPlayerButton.setImageResource(R.drawable.av_play_light);
						}
					}
					else {
						holder.mPlayerButton.setChecked(false);
						holder.mPlayerButton.setImageResource(R.drawable.av_play_light);

						callbackStateNextButton(true);
						holder.mCheckRingtone.setChecked(false);
						holder.mVolumeControl.setVisibility(View.GONE);
					}
				}
				else {
					holder.mPlayerButton.setChecked(false);
					holder.mPlayerButton.setImageResource(R.drawable.av_play_light);

					holder.mCheckRingtone.setChecked(false);
					holder.mVolumeControl.setVisibility(View.GONE);
				}
			}
			return view;
		}
	}
}

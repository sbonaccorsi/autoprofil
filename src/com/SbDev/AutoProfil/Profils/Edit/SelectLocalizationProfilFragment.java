package com.SbDev.AutoProfil.Profils.Edit;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.Profils.Model.PageFragment;
import com.SbDev.AutoProfil.data.Local.ProfilManagement;
import com.SbDev.AutoProfil.data.provider.AutoProfilContent.Profils;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

interface OnMarkerAdded {
	void onMarkerAddedOnMap(Marker marker);
}

public class SelectLocalizationProfilFragment extends PageFragment implements OnMarkerAdded {

	private ActionBar					  mActionBar;	

	private LocalizationProfilMapFragment mMapFragment;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_select_localization_profil, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mMapFragment = (LocalizationProfilMapFragment) getFragmentManager().findFragmentByTag(LocalizationProfilMapFragment.TAG_MAP_FRAGMENT);

		if (mMapFragment == null) {
			mMapFragment = LocalizationProfilMapFragment.newInstance(this);
			getFragmentManager().beginTransaction()
			.add(R.id.container_map_google, mMapFragment, LocalizationProfilMapFragment.TAG_MAP_FRAGMENT).commit();
		}
		else {
			getFragmentManager().beginTransaction().remove(mMapFragment).commit();
			mMapFragment = null;
			mMapFragment = LocalizationProfilMapFragment.newInstance(this);
			getFragmentManager().beginTransaction()
			.add(R.id.container_map_google, mMapFragment, LocalizationProfilMapFragment.TAG_MAP_FRAGMENT).commit();
		}
	}

	@Override
	protected void enterThePage() {

		if (getActivity() instanceof EditProfilActivity) {
			((EditProfilActivity)getActivity()).displaySearchView(true);
		}
		
		mActionBar = ((SherlockFragmentActivity)getActivity()).getSupportActionBar();
		mActionBar.setListNavigationCallbacks(null, null);
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		
		mMapFragment.initCameraPosition();
		
		if (mIsForced && ProfilManagement.getInstance(getActivity().getApplicationContext()).getValue(Profils.Columns.LATITUDE.getName()) == null &&
				ProfilManagement.getInstance(getActivity().getApplicationContext()).getValue(Profils.Columns.LONGITUDE.getName()) == null)
			mListenerObserver.enableNextButton(false);
		else {
			mListenerObserver.enableNextButton(true);
			String latitude = (String) ProfilManagement.getInstance(getActivity().getApplicationContext()).getValue(Profils.Columns.LATITUDE.getName());
			String longitude = (String) ProfilManagement.getInstance(getActivity().getApplicationContext()).getValue(Profils.Columns.LONGITUDE.getName());
			if (latitude != null && latitude.length() != 0 &&
					longitude != null && longitude.length() != 0) {
				double lat = Double.valueOf(latitude);
				double lon = Double.valueOf(longitude);
				mMapFragment.putExistingMarker(new LatLng(lat, lon));
			}
		}
	}

	@Override
	protected void outOfPage() {
		if (getActivity() instanceof EditProfilActivity) {
			((EditProfilActivity)getActivity()).displaySearchView(false);
			((EditProfilActivity)getActivity()).getSearchView().clearFocus();
		}
	}

	@Override
	public void onMarkerAddedOnMap(Marker marker) {
		mListenerObserver.enableNextButton(true);
	}
}

package com.SbDev.AutoProfil.Profils.Edit;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.Profils.Model.PageFragment;
import com.SbDev.AutoProfil.data.Local.DataInterface.ProfilConstant;
import com.SbDev.AutoProfil.data.Local.ProfilManagement;
import com.SbDev.AutoProfil.data.provider.AutoProfilContent.Profils;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.bonadevapps.compoments.widgets.SeekBarChangeColor;

public class SelectBrightnessFragment extends PageFragment implements OnCheckedChangeListener, OnSeekBarChangeListener, OnItemSelectedListener {

	private ActionBar						mActionBar;

	private List<String> 					mTabSleepTime;

	private Integer							mCurrentValueBrigtness;
	private Integer							mCurrentMode;

	private TextView						mCurrentBright;
	private CheckBox						mCheckAuto;
	private SeekBar							mSeekBright;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_select_brightness, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		try {
			mCurrentValueBrigtness = System.getInt(getActivity().getContentResolver(), System.SCREEN_BRIGHTNESS);
			mCurrentMode = System.getInt(getActivity().getContentResolver(), System.SCREEN_BRIGHTNESS_MODE);
		} catch (SettingNotFoundException e) {
			e.printStackTrace();
		}

		mCurrentBright = (TextView) view.findViewById(R.id.textView_current_bright);
		mCheckAuto = (CheckBox) view.findViewById(R.id.checkBox_active_auto);
		mSeekBright = (SeekBar) view.findViewById(R.id.seekBar_bright);

		mSeekBright.setMax(255);
		mSeekBright.setProgress(mCurrentValueBrigtness);
		mCurrentBright.setText(Integer.toString(mCurrentValueBrigtness));

		mCheckAuto.setOnCheckedChangeListener(this);
		mSeekBright.setOnSeekBarChangeListener(this);

		mTabSleepTime = new ArrayList<String>();
		mTabSleepTime.add(getString(R.string.time_sleep_15_seconds));
		mTabSleepTime.add(getString(R.string.time_sleep_30_seconds));
		mTabSleepTime.add(getString(R.string.time_sleep_1_minute));
		mTabSleepTime.add(getString(R.string.time_sleep_2_minutes));
		mTabSleepTime.add(getString(R.string.time_sleep_5_minutes));
		mTabSleepTime.add(getString(R.string.time_sleep_10_minutes));
		mTabSleepTime.add(getString(R.string.time_sleep_30_minutes));

		SpinnerAdapterTimeSleep adapter = new SpinnerAdapterTimeSleep();
		((Spinner)view.findViewById(R.id.spinner_time_screen_off)).setAdapter(adapter);
		((Spinner)view.findViewById(R.id.spinner_time_screen_off)).setOnItemSelectedListener(this);
	}

	@Override
	public void onNothingSelected(AdapterView<?> adp) {

	}

	@Override
	public void onItemSelected(AdapterView<?> adp, View view, int position, long id) {
		switch (position) {
		case 0:
			ProfilManagement.getInstance(getActivity().getApplicationContext())
			.putIntegerValue(Profils.Columns.SLEEP_TIME_SCREEN.getName(), ProfilConstant.SLEEP_TIME_15000);
			break;
		case 1:
			ProfilManagement.getInstance(getActivity().getApplicationContext())
			.putIntegerValue(Profils.Columns.SLEEP_TIME_SCREEN.getName(), ProfilConstant.SLEEP_TIME_30000);
			break;
		case 2:
			ProfilManagement.getInstance(getActivity().getApplicationContext())
			.putIntegerValue(Profils.Columns.SLEEP_TIME_SCREEN.getName(), ProfilConstant.SLEEP_TIME_60000);
			break;
		case 3:
			ProfilManagement.getInstance(getActivity().getApplicationContext())
			.putIntegerValue(Profils.Columns.SLEEP_TIME_SCREEN.getName(), ProfilConstant.SLEEP_TIME_120000);
			break;
		case 4:
			ProfilManagement.getInstance(getActivity().getApplicationContext())
			.putIntegerValue(Profils.Columns.SLEEP_TIME_SCREEN.getName(), ProfilConstant.SLEEP_TIME_360000);
			break;
		case 5:
			ProfilManagement.getInstance(getActivity().getApplicationContext())
			.putIntegerValue(Profils.Columns.SLEEP_TIME_SCREEN.getName(), ProfilConstant.SLEEP_TIME_720000);
			break;
		case 6:
			ProfilManagement.getInstance(getActivity().getApplicationContext())
			.putIntegerValue(Profils.Columns.SLEEP_TIME_SCREEN.getName(), ProfilConstant.SLEEP_TIME_2160000);
			break;
		}		
	}

	@Override
	protected void enterThePage() {
		mActionBar = ((SherlockFragmentActivity)getActivity()).getSupportActionBar();
		mActionBar.setListNavigationCallbacks(null, null);
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

		Spinner spinner = (Spinner)getView().findViewById(R.id.spinner_time_screen_off);

		Integer time = (Integer) ProfilManagement.getInstance(getActivity().getApplicationContext()).getValue(Profils.Columns.SLEEP_TIME_SCREEN.getName());
		try {
			if (time == null) {
				time = System.getInt(getActivity().getContentResolver(), System.SCREEN_OFF_TIMEOUT);
			}
			
			int position = this.getSpinnerPositionByTime(time);
			spinner.setSelection(position, true);
			
		} catch (SettingNotFoundException e) {
			e.printStackTrace();
		}

		Integer mode = (Integer) ProfilManagement.getInstance(getActivity().getApplicationContext()).getValue(Profils.Columns.BRIGTNESS_MODE.getName());
		if (mode != null && mode == ProfilConstant.TYPE_BRIGTNESS_AUTO) {
			mCheckAuto.setChecked(true);
		}
		else if (mode != null && mode == ProfilConstant.TYPE_BRIGTNESS_MANUAL) {
			mCheckAuto.setChecked(false);
			Integer value = (Integer) ProfilManagement.getInstance(getActivity().getApplicationContext()).getValue(Profils.Columns.BRIGTNESS_INTENSITY.getName());
			if (value != null) {
				mSeekBright.setProgress(value);
			}
		}

		if (mIsForced) {
			mListenerObserver.enableNextButton(false);
		}
		else
			mListenerObserver.enableNextButton(true);
	}

	@Override
	protected void outOfPage() {

		if (mCheckAuto.isChecked()) {
			ProfilManagement.getInstance(getActivity().getApplicationContext())
			.putIntegerValue(Profils.Columns.BRIGTNESS_MODE.getName(), ProfilConstant.TYPE_BRIGTNESS_AUTO);
		}
		else {
			ProfilManagement.getInstance(getActivity().getApplicationContext())
			.putIntegerValue(Profils.Columns.BRIGTNESS_MODE.getName(), ProfilConstant.TYPE_BRIGTNESS_MANUAL);
			ProfilManagement.getInstance(getActivity().getApplicationContext())
			.putIntegerValue(Profils.Columns.BRIGTNESS_INTENSITY.getName(), Integer.valueOf(mCurrentBright.getText().toString()));
		}
		this.restoreStateBrigthness();
	}

	public void restoreStateBrigthness() {
		System.putInt(getActivity().getContentResolver(), System.SCREEN_BRIGHTNESS, mCurrentValueBrigtness);
		System.putInt(getActivity().getContentResolver(), System.SCREEN_BRIGHTNESS_MODE, mCurrentMode);
	}
	
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

		if (isChecked) {
			mSeekBright.setEnabled(false);
			mCurrentBright.setText("Automatic");
			ProfilManagement.getInstance(getActivity().getApplicationContext())
			.putIntegerValue(Profils.Columns.BRIGTNESS_MODE.getName(), ProfilConstant.TYPE_BRIGTNESS_AUTO);
			System.putInt(getActivity().getContentResolver(), System.SCREEN_BRIGHTNESS_MODE, System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC);
			ProfilManagement.getInstance(getActivity().getApplicationContext())
			.putIntegerValue(Profils.Columns.BRIGTNESS_INTENSITY.getName(), -1);
		}
		else {
			mSeekBright.setEnabled(true);
			mCurrentBright.setText(Integer.toString(mSeekBright.getProgress()));
			ProfilManagement.getInstance(getActivity().getApplicationContext())
			.putIntegerValue(Profils.Columns.BRIGTNESS_MODE.getName(), ProfilConstant.TYPE_BRIGTNESS_MANUAL);
			ProfilManagement.getInstance(getActivity().getApplicationContext())
			.putIntegerValue(Profils.Columns.BRIGTNESS_INTENSITY.getName(), Integer.valueOf(mCurrentBright.getText().toString()));

			System.putInt(getActivity().getContentResolver(), System.SCREEN_BRIGHTNESS_MODE, System.SCREEN_BRIGHTNESS_MODE_MANUAL);
		}
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		mCurrentBright.setText(Integer.toString(progress));
		ProfilManagement.getInstance(getActivity().getApplicationContext())
		.putIntegerValue(Profils.Columns.BRIGTNESS_INTENSITY.getName(), progress);
		System.putInt(getActivity().getContentResolver(), System.SCREEN_BRIGHTNESS, progress);
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
	}

	private int getSpinnerPositionByTime(int time) {
		switch (time) {
		case ProfilConstant.SLEEP_TIME_15000:
			return 0;
		case ProfilConstant.SLEEP_TIME_30000:
			return 1;
		case ProfilConstant.SLEEP_TIME_60000:
			return 2;
		case ProfilConstant.SLEEP_TIME_120000:
			return 3;
		case ProfilConstant.SLEEP_TIME_360000:
			return 4;
		case ProfilConstant.SLEEP_TIME_720000:
			return 5;
		case ProfilConstant.SLEEP_TIME_2160000:
			return 6;
		}
		return -1;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		this.restoreStateBrigthness();
	}
	
	private class ViewHolderTimeSleep {

		public TextView mTime;

		public ViewHolderTimeSleep(View v) {
			mTime = (TextView) v.findViewById(R.id.textView_time_sleep);
		}
	}

	private class SpinnerAdapterTimeSleep extends BaseAdapter {

		@Override
		public int getCount() {
			return mTabSleepTime.size();
		}

		@Override
		public Object getItem(int position) {
			return mTabSleepTime.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			ViewHolderTimeSleep holder = null;

			if (convertView == null) {
				convertView = LayoutInflater.from(getActivity().getBaseContext()).inflate(R.layout.cell_time_screen_sleep, null);
				holder = new ViewHolderTimeSleep(convertView);
				convertView.setTag(holder);
			}
			else {
				holder = (ViewHolderTimeSleep) convertView.getTag();
			}

			holder.mTime.setText(mTabSleepTime.get(position));

			return convertView;
		}

	}
}

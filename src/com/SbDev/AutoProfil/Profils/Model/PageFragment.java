package com.SbDev.AutoProfil.Profils.Model;

import com.actionbarsherlock.app.SherlockFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class PageFragment extends Fragment {

	public static final String							EXTRA_PARAM_IS_FORCED_PAGE = "isForced";
	public static final String							EXTRA_FNAME = "fname";

	protected StateNavigationObserver 					mListenerObserver;
	protected SelectBranchObserver 						mListenerBranchObserver;

	protected Boolean									mIsForced;
	
	protected String									mFname;

	public static PageFragment instantiate(Context context, String fname, Bundle param, StateNavigationObserver listener) {
		PageFragment fragment = (PageFragment) PageFragment.instantiate(context, fname, param);
		fragment.setStateNavigationObserver(listener);
		fragment.setFName(fname);
		return fragment;
	}

	public static PageFragment instantiate(Context context, String fname, Bundle param, StateNavigationObserver listenerStatePage, 
			SelectBranchObserver listenerSelectBranch) {
		PageFragment fragment = (PageFragment) PageFragment.instantiate(context, fname, param);
		fragment.setStateNavigationObserver(listenerStatePage);
		fragment.setSelectBranchObserver(listenerSelectBranch);
		fragment.setFName(fname);
		return fragment;
	}

	public void setFName(String name) {
		mFname = name;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		if (getArguments() != null) {
			if (getArguments().containsKey(EXTRA_PARAM_IS_FORCED_PAGE))
				mIsForced = getArguments().getBoolean(EXTRA_PARAM_IS_FORCED_PAGE);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}
	
	abstract protected void enterThePage();
	
	abstract protected void outOfPage();


	public void setStateNavigationObserver(StateNavigationObserver listener) {
		mListenerObserver = listener;
	}

	public void setSelectBranchObserver(SelectBranchObserver listener) {
		mListenerBranchObserver = listener;
	}

	protected void callbackStateNextButton(Boolean value) {
		mListenerObserver.enableNextButton(value);
	}

	protected void callbackSelectBranch(Integer branch) {
		mListenerBranchObserver.SetCurrentBranchSelected(branch);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}
}

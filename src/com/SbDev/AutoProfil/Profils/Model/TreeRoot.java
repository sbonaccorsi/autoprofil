package com.SbDev.AutoProfil.Profils.Model;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

public class TreeRoot implements SelectBranchObserver {

	private Context mContext;
	private FragmentManager mFragManager;
	private ViewPager mViewPager;
	private ViewPagerAdapter mAdapter;

	private Page mRootPage;

	private int mCurrentPage;
	private int mCurrentBranch = 0;
	private int mMaxPageInBranch;

	private List<Fragment> mListPager;
	private List<BranchPage> mListBranch;

	public TreeRoot(Context context, ViewPager viewPager, FragmentManager fm) {
		mContext = context;
		mViewPager = viewPager;
		mFragManager = fm;
		mCurrentPage = Integer.valueOf(0);

		mListBranch = new ArrayList<BranchPage>();
		mListPager = new ArrayList<Fragment>();
	}

	public void createTree(Page root, BranchPage... branchs) {

		mRootPage = root;
		mListPager.add(mRootPage.getFragment());
		mRootPage.getFragment().enterThePage();
		mRootPage.setSelectBranchObserverListener(this);
		for (int idx = 0; idx < branchs.length; ++idx) {
			branchs[idx].addRootPage(mRootPage);
			mListBranch.add(branchs[idx]);
		}
		mAdapter = new ViewPagerAdapter(mFragManager);
		mAdapter.addFragment(mRootPage.getFragment());
		mViewPager.setAdapter(mAdapter);
	}

	public List<BranchPage> getListBranch() {
		return mListBranch;
	}

	public void nextPage() {

		List<Page> listBranch = mListBranch.get(mCurrentBranch).getListPageInBranch();

		if (mCurrentPage < listBranch.size()) {
			if (!mListPager.contains(listBranch.get(mCurrentPage))) {

				listBranch.get(mCurrentPage).getFragment().outOfPage();
				mCurrentPage += 1;
				
				Page pageBranch = listBranch.get(mCurrentPage);
				mListPager.add(pageBranch.getFragment());
				mAdapter.addFragment(pageBranch.getFragment());
				
				listBranch.get(mCurrentPage).getFragment().enterThePage();
			}
			mViewPager.setCurrentItem(mCurrentPage, true);
		}
	}

	public void prevPage() {

		List<Page> listBranch = mListBranch.get(mCurrentBranch).getListPageInBranch();

		if (mCurrentPage >= 0) {

			listBranch.get(mCurrentPage).getFragment().outOfPage();
			mCurrentPage--;
			listBranch.get(mCurrentPage).getFragment().enterThePage();
			mViewPager.setCurrentItem(mCurrentPage, true);
		}
	}

	public void logTreePage() {

		Log.e("PAGE ROOT", "NAME " + mRootPage.getNamePage());
		for (int idx = 0; idx < mListBranch.size(); ++idx) {

			Log.e("BRANCH", "NUMBER " + idx);
			for (int idx2 = 0; idx2 < mListBranch.get(idx).getListPageInBranch().size(); ++idx2) {
				Page page = mListBranch.get(idx).getListPageInBranch().get(idx2);
				Log.e("PAGE", "NAME " + page.getNamePage());
			}
		}
	}

	@Override
	public void SetCurrentBranchSelected(int branch) {

		Log.e("SetCurrentBranchSelected", "SetCurrentBranchSelected");
		if (mCurrentPage == 0) {
			if (mCurrentBranch != branch && mListPager.size() > 1) {
				List<Page> list = mListBranch.get(mCurrentBranch).getListPageInBranch();
				for (int idx = 1; idx < list.size(); ++idx) {
					if (mListPager.remove(list.get(idx).getFragment()) == true) {
						mAdapter.removeFragment(list.get(idx).getFragment());
						list.get(idx).destroyPage();
					}
				}
				mViewPager.invalidate();
				mViewPager.setCurrentItem(0, true);
			}
			mCurrentBranch = branch;
		}
	}

	class ViewPagerAdapter extends FragmentStatePagerAdapter {

		private List<Fragment> mList;
		private FragmentManager mFragmentManager;

		public ViewPagerAdapter(FragmentManager fm) {
			super(fm);
			mList = new ArrayList<Fragment>();
			mFragmentManager = fm;
		}

		public void removeFragment(Fragment fragment) {

			Log.e("removeFragment", "removeFragment");
			if (mList.remove(fragment) == true) {
				fragment = null;
				notifyDataSetChanged();
			}
		}

		public void addFragment(Fragment fragment) {
			if (mList.contains(fragment) == false) {
				if (mList.add(fragment) == true)
					notifyDataSetChanged();
			}
		}

		@Override
		public int getItemPosition(Object object) {
			if (mList.contains(object)) {
				return POSITION_UNCHANGED;
			}
			return POSITION_NONE;
		}

		@Override
		public Fragment getItem(int position) {
			return mList.get(position);
		}

		@Override
		public int getCount() {
			return mList.size();
		}
	}
}

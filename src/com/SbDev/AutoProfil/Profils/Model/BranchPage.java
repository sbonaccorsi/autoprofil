package com.SbDev.AutoProfil.Profils.Model;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

public class BranchPage {

	private Context mContext;
	
	private List<Page> mListPage;
	
	public BranchPage(Context context) {
		mContext = context;
		mListPage = new ArrayList<Page>();
	}
	
	public void addRootPage(Page pageRoot) {
		mListPage.add(0, pageRoot);
	}
	
	public BranchPage addPageToBranch(Page... pages) {
		
		for (int idx = 0; idx < pages.length; ++idx) {
			mListPage.add(pages[idx]);
		}
		return this;
	}
	
	public List<Page> getListPageInBranch() {
		return mListPage;
	}
}

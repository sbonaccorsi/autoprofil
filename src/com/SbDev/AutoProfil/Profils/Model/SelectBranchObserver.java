package com.SbDev.AutoProfil.Profils.Model;

public interface SelectBranchObserver {

	void SetCurrentBranchSelected(int branch);
}

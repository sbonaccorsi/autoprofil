package com.SbDev.AutoProfil.Profils.Model;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class Page {

	private Context 							mContext;
	private String 								mNameFragment;
	private PageFragment 						mFragment;
	private boolean 							mPageComplete = false;
	private StateNavigationObserver 			mListenerCompletePage;
	private Boolean								mIsForcedPage;
	private Bundle								mParamFragment;

	public Page(Context context, String fname, StateNavigationObserver listener, Boolean isForcedPage, Bundle paramFragment) {
		mContext = context;
		mNameFragment = fname;
		mListenerCompletePage = listener;
		mIsForcedPage = isForcedPage;
		mParamFragment = paramFragment;
		if (mParamFragment == null) {
			mParamFragment = new Bundle();
		}
		mParamFragment.putBoolean(PageFragment.EXTRA_PARAM_IS_FORCED_PAGE, mIsForcedPage);
	}

	@Deprecated
	public Page(Context context, String fname, Bundle param, StateNavigationObserver listener) {
		mContext = context;
		mNameFragment = fname;
		mListenerCompletePage = listener;
	}

	public void setSelectBranchObserverListener(SelectBranchObserver listener) {
		if (listener != null) {
			mFragment.setSelectBranchObserver(listener);
		}
	}

	private void instanciateFragment() {

		if (mFragment == null) {
			mFragment = (PageFragment) PageFragment.instantiate(mContext, mNameFragment, mParamFragment, mListenerCompletePage);
		}
	}

	public PageFragment getFragment() {

		if (mFragment == null)
			this.instanciateFragment();
		return mFragment;
	}

	public String getNamePage() {
		return mNameFragment;
	}

	public boolean getStatePage() {
		return mPageComplete;
	}

	public void destroyPage() {
		mFragment = null;
	}
}

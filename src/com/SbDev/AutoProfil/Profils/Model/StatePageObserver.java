package com.SbDev.AutoProfil.Profils.Model;

import java.io.Serializable;

public interface StatePageObserver {

	void PageIsCompleted();
	void PageNotCompleted();
}

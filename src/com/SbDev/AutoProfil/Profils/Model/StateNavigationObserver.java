package com.SbDev.AutoProfil.Profils.Model;

public interface StateNavigationObserver {

	void enableNextButton(boolean complete);
	void renameNextButton(String name);
}

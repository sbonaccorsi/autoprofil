package com.SbDev.AutoProfil.data.provider.util;

public interface ColumnMetadata {

    public int getIndex();

    public String getName();

    public String getType();
}

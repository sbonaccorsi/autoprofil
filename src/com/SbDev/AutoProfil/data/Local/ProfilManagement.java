package com.SbDev.AutoProfil.data.Local;

import com.SbDev.AutoProfil.data.Local.DataInterface.ProfilConstant;
import com.SbDev.AutoProfil.data.provider.AutoProfilContent.Profils;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

public class ProfilManagement {

	private static ProfilManagement mInstance;
	
	private Context mContext;
	
	private ContentValues mValues;
	
	public static ProfilManagement getInstance(Context context) {
		
		if (mInstance == null)
			mInstance = new ProfilManagement(context);
		return mInstance;
	}
	
	public ProfilManagement(Context context) {
		mContext = context;
		mValues = new ContentValues();
	}
	
	public void createFromProfil(Profil profil) {
		this.clearValues();
		
		this.putIntegerValue(Profils.Columns.ID.getName(), profil.id);
		
		this.putStringValue(Profils.Columns.PROFIL_NAME.getName(), profil.name);
		
		this.putIntegerValue(Profils.Columns.MODE.getName(), profil.mode);
		
		this.putIntegerValue(Profils.Columns.RINGTONE_ID.getName(), profil.ringtoneId);
		this.putIntegerValue(Profils.Columns.NOTIFICATION_RINGTONE_LEVEL.getName(), profil.ringtoneVolume);
		this.putIntegerValue(Profils.Columns.RINGTONE_CATEGORY.getName(), profil.ringtoneCategory);
		this.putStringValue(Profils.Columns.RINGTONE_DATA.getName(), profil.ringtonePath);
		this.putIntegerValue(Profils.Columns.NOTIFICATION_ID.getName(), profil.notificationId);
		this.putIntegerValue(Profils.Columns.NOTIFICATION_CATEGORY.getName(), profil.notificationCategory);
		this.putStringValue(Profils.Columns.NOTIFICATION_DATA.getName(), profil.notificationPath);
		
		this.putIntegerValue(Profils.Columns.SOUND_LEVEL.getName(), profil.soundVolume);
		
		this.putBooleanValue(Profils.Columns.DIAL_PAD_TONES.getName(), profil.dialPadTouchTones);
		this.putBooleanValue(Profils.Columns.TOUCH_SOUNDS.getName(), profil.touchSounds);
		this.putBooleanValue(Profils.Columns.SCREEN_LOCK_SOUNDS.getName(), profil.screenLockSounds);
		this.putBooleanValue(Profils.Columns.VIBRATE_TOUCH.getName(), profil.vibrateTouch);
		
		this.putIntegerValue(Profils.Columns.BRIGTNESS_MODE.getName(), profil.brightnessMode);
		if (profil.brightnessMode == ProfilConstant.TYPE_BRIGTNESS_MANUAL)
			this.putIntegerValue(Profils.Columns.BRIGTNESS_INTENSITY.getName(), profil.brightnessIntensity);
		
		this.putIntegerValue(Profils.Columns.SLEEP_TIME_SCREEN.getName(), profil.sleepTimeScreen);
		
		this.putIntegerValue(Profils.Columns.BACKGROUND_ID.getName(), profil.backgroundId);
		this.putStringValue(Profils.Columns.BACKGROUND_DATA.getName(), profil.backgroundPath);
		
		this.putStringValue(Profils.Columns.LATITUDE.getName(), profil.latitude);
		this.putStringValue(Profils.Columns.LONGITUDE.getName(), profil.longitude);
		this.putStringValue(Profils.Columns.ADDRESS.getName(), profil.address);
		
		this.putBooleanValue(Profils.Columns.ACTIVE.getName(), profil.active);
		this.putBooleanValue(Profils.Columns.IS_SET.getName(), profil.isSet);
		
		this.putBooleanValue(Profils.Columns.WIFI.getName(), profil.wifi);
		this.putBooleanValue(Profils.Columns.WIFI_DIRECT.getName(), profil.wifiDirect);
		this.putBooleanValue(Profils.Columns.BLUETHOOT.getName(), profil.bluethoot);
		this.putBooleanValue(Profils.Columns.NFC.getName(), profil.nfc);
		this.putBooleanValue(Profils.Columns.DEBUG_MODE.getName(), profil.debugMode);
	}
	
	public void putIntegerValue(String key, Integer value) {
		mValues.put(key, value);
//		Log.e("[Profil]", "--- Insert integer key = " + key + " value = " + value);
//		Log.e("[Profil]", "--- ContentValue size " + mValues.size());
	}
	
	public void putStringValue(String key, String value) {
		mValues.put(key, value);
//		Log.e("[Profil]", "--- Insert string key = " + key + " value = " + value);
//		Log.e("[Profil]", "--- ContentValue size " + mValues.size());
	}
	
	public void putBooleanValue(String key, Boolean value) {
		mValues.put(key, value);
//		Log.e("[Profil]", "--- Insert string key = " + key + " value = " + value);
//		Log.e("[Profil]", "--- ContentValue size " + mValues.size());
	}
	
	public Object getValue(String key) {
		return mValues.get(key);
	}
	
	public void removeValue(String key) {
		mValues.remove(key);
//		Log.e("[Profil]", "--- Remove value key = " + key);
//		Log.e("[Profil]", "--- ContentValue size " + mValues.size());
	}
	
	public void clearValues() {
		mValues.clear();
	}
	
	public void insertProfilValuesInDatabase() {
		mContext.getContentResolver().insert(Profils.CONTENT_URI, mValues);
		this.clearValues();
	}
	
	public void updateProfilValuesInDatabase(String where, String[] selectionArgs) {
		mContext.getContentResolver().update(Profils.CONTENT_URI, mValues, where, selectionArgs);
		this.clearValues();
	}
}

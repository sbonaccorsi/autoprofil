package com.SbDev.AutoProfil.data.Local;

import android.content.Context;
import android.database.Cursor;
import com.SbDev.AutoProfil.data.provider.AutoProfilContent.Profils;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

public class MarkerData {

	public Marker				marker;
	public Circle				circle;
	
	public Integer				id;
	public String				tag;
	public LatLng				coordinate;
	public String				name;
	public Integer				mode;
	public Integer				imageId;
	public String				image;
	
	private MarkerData(Context ctx, Cursor cursor) {
		
		id = cursor.getInt(cursor.getColumnIndex(Profils.Columns.ID.getName()));
		tag = cursor.getString(cursor.getColumnIndex(Profils.Columns.LATITUDE.getName())) 
				+ cursor.getString(cursor.getColumnIndex(Profils.Columns.LONGITUDE.getName()));
		Double lat = Double.valueOf(cursor.getString(cursor.getColumnIndex(Profils.Columns.LATITUDE.getName())));
		Double lon = Double.valueOf(cursor.getString(cursor.getColumnIndex(Profils.Columns.LONGITUDE.getName())));
		coordinate = new LatLng(lat, lon);
		name = cursor.getString(cursor.getColumnIndex(Profils.Columns.PROFIL_NAME.getName()));
		mode = cursor.getInt(cursor.getColumnIndex(Profils.Columns.MODE.getName()));
		image = cursor.getString(cursor.getColumnIndex(Profils.Columns.BACKGROUND_DATA.getName()));
		imageId = cursor.getInt(cursor.getColumnIndex(Profils.Columns.BACKGROUND_ID.getName()));
	}
	
	public static MarkerData createFromCursor(Context ctx, Cursor cursor) {
		MarkerData data = new MarkerData(ctx, cursor);
		return data;
	}
	
	public static MarkerData createFromMarker(Context ctx, Marker marker) {
		
		Cursor cursor = ctx.getContentResolver().query(Profils.CONTENT_URI, 
				Profils.PROJECTION, Profils.Columns.LATITUDE.getName() + "=? AND " + Profils.Columns.LONGITUDE.getName() + "=?", 
				new String[]{Double.toString(marker.getPosition().latitude), Double.toString(marker.getPosition().longitude)}, null);
		if (cursor.moveToFirst()) {
			return new MarkerData(ctx, cursor);
		}
		else {
			return null;
		}
	}
}

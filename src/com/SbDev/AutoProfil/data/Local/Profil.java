package com.SbDev.AutoProfil.data.Local;

import java.io.IOException;

import yuku.iconcontextmenu.IconContextMenu;
import android.app.WallpaperManager;
import android.bluetooth.BluetoothAdapter;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.provider.MediaStore.Audio.AudioColumns;
import android.provider.MediaStore.Audio.Media;
import android.provider.Settings.System;
import android.sax.StartElementListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;

import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.Profils.Edit.EditProfilActivity;
import com.SbDev.AutoProfil.data.Local.DataInterface.ProfilConstant;
import com.SbDev.AutoProfil.data.provider.AutoProfilContent.Profils;

public class Profil {

	private Context 		mContext;

	public Integer 			id;
	public String 			name;
	public Integer 			mode;

	public Integer 			ringtoneId;
	public Integer 			ringtoneVolume;
	public Integer 			ringtoneCategory;
	public String 			ringtonePath;

	public Integer 			notificationId;
	public String 			notificationPath;
	public Integer 			notificationCategory;

	public Integer 			soundVolume;

	public Boolean 			dialPadTouchTones;
	public Boolean 			touchSounds;
	public Boolean 			screenLockSounds;
	public Boolean 			vibrateTouch;

	public Integer 			brightnessMode;
	public Integer 			brightnessIntensity;

	public Integer			sleepTimeScreen;

	public Integer 			backgroundId;
	public String 			backgroundPath;

	public String 			latitude;
	public String 			longitude;
	public String 			address;

	public Boolean 			active;
	public Boolean 			isSet;

	public Boolean 			wifi;
	public Boolean 			wifiDirect;
	public Boolean 			bluethoot;
	public Boolean 			nfc;
	public Boolean 			debugMode;

	private Profil(Context ctx, Cursor cursor) {

		mContext = ctx;
		this.initProfil(cursor);
	}

	public static Profil createFromCursor(Context ctx, Cursor cursor) {

		return new Profil(ctx, cursor);
	}

	public static Profil createFromId(Context ctx, Integer id) {
		Cursor cursor = ctx.getContentResolver().query(Profils.CONTENT_URI, Profils.PROJECTION, Profils.Columns.ID.getName() + "=?",
				new String[]{Integer.toString(id)}, null);
		if (cursor.moveToFirst())
			return createFromCursor(ctx, cursor);
		cursor.close();
		return null;
	}

	private void initProfil(Cursor cursor) {

		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.ID.getName())))
			id = cursor.getInt(cursor.getColumnIndex(Profils.Columns.ID.getName()));
		else
			id = null;

		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.PROFIL_NAME.getName())))
			name = cursor.getString(cursor.getColumnIndex(Profils.Columns.PROFIL_NAME.getName()));
		else
			name = null;
		//		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.MODE.getName())))
		mode = cursor.getInt(cursor.getColumnIndex(Profils.Columns.MODE.getName()));
		//		else
		//			mode = null;

		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.RINGTONE_ID.getName())))
			ringtoneId = cursor.getInt(cursor.getColumnIndex(Profils.Columns.RINGTONE_ID.getName()));
		else
			ringtoneId = null;
		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.RINGTONE_CATEGORY.getName())))
			ringtoneCategory = cursor.getInt(cursor.getColumnIndex(Profils.Columns.RINGTONE_CATEGORY.getName()));
		else
			ringtoneCategory = null;
		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.RINGTONE_DATA.getName())))
			ringtonePath = cursor.getString(cursor.getColumnIndex(Profils.Columns.RINGTONE_DATA.getName()));
		else
			ringtonePath = null;
		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.NOTIFICATION_RINGTONE_LEVEL.getName())))
			ringtoneVolume = cursor.getInt(cursor.getColumnIndex(Profils.Columns.NOTIFICATION_RINGTONE_LEVEL.getName()));
		else
			ringtoneVolume = null;
		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.NOTIFICATION_ID.getName())))
			notificationId = cursor.getInt(cursor.getColumnIndex(Profils.Columns.NOTIFICATION_ID.getName()));
		else
			notificationId = null;
		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.NOTIFICATION_CATEGORY.getName())))
			notificationCategory = cursor.getInt(cursor.getColumnIndex(Profils.Columns.NOTIFICATION_CATEGORY.getName()));
		else
			notificationCategory = null;
		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.NOTIFICATION_DATA.getName())))
			notificationPath = cursor.getString(cursor.getColumnIndex(Profils.Columns.NOTIFICATION_DATA.getName()));
		else
			notificationPath = null;

		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.SOUND_LEVEL.getName())))
			soundVolume = cursor.getInt(cursor.getColumnIndex(Profils.Columns.SOUND_LEVEL.getName()));
		else
			soundVolume = null;

		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.DIAL_PAD_TONES.getName())))
			dialPadTouchTones = cursor.getInt(cursor.getColumnIndex(Profils.Columns.DIAL_PAD_TONES.getName())) == 1 ? true : false;
		else
			dialPadTouchTones = null;

		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.TOUCH_SOUNDS.getName())))
			touchSounds = cursor.getInt(cursor.getColumnIndex(Profils.Columns.TOUCH_SOUNDS.getName())) == 1 ? true : false;
		else
			touchSounds = null;

		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.SCREEN_LOCK_SOUNDS.getName())))
			screenLockSounds = cursor.getInt(cursor.getColumnIndex(Profils.Columns.SCREEN_LOCK_SOUNDS.getName())) == 1 ? true : false;
		else
			screenLockSounds = null;

		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.VIBRATE_TOUCH.getName())))
			vibrateTouch = cursor.getInt(cursor.getColumnIndex(Profils.Columns.VIBRATE_TOUCH.getName())) == 1 ? true : false;
		else
			vibrateTouch = null;

		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.BRIGTNESS_MODE.getName())))
			brightnessMode = cursor.getInt(cursor.getColumnIndex(Profils.Columns.BRIGTNESS_MODE.getName()));
		else
			brightnessMode = null;
		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.BRIGTNESS_INTENSITY.getName())))
			brightnessIntensity = cursor.getInt(cursor.getColumnIndex(Profils.Columns.BRIGTNESS_INTENSITY.getName()));
		else
			brightnessIntensity = null;

		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.SLEEP_TIME_SCREEN.getName())))
			sleepTimeScreen = cursor.getInt(cursor.getColumnIndex(Profils.Columns.SLEEP_TIME_SCREEN.getName()));
		else
			sleepTimeScreen = null;

		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.BACKGROUND_ID.getName())))
			backgroundId = cursor.getInt(cursor.getColumnIndex(Profils.Columns.BACKGROUND_ID.getName()));
		else
			backgroundId = null;
		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.BACKGROUND_DATA.getName())))
			backgroundPath = cursor.getString(cursor.getColumnIndex(Profils.Columns.BACKGROUND_DATA.getName()));
		else
			backgroundPath = null;

		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.LATITUDE.getName())))
			latitude = cursor.getString(cursor.getColumnIndex(Profils.Columns.LATITUDE.getName()));
		else
			latitude = null;
		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.LONGITUDE.getName())))
			longitude = cursor.getString(cursor.getColumnIndex(Profils.Columns.LONGITUDE.getName()));
		else
			longitude = null;
		if (!cursor.isNull(cursor.getColumnIndex(Profils.Columns.ADDRESS.getName())))
			address = cursor.getString(cursor.getColumnIndex(Profils.Columns.ADDRESS.getName()));
		else
			address = null;

		active = cursor.getInt(cursor.getColumnIndex(Profils.Columns.ACTIVE.getName())) == 1 ? true : false;
		isSet = cursor.getInt(cursor.getColumnIndex(Profils.Columns.IS_SET.getName())) == 1 ? true : false;

		wifi = cursor.getInt(cursor.getColumnIndex(Profils.Columns.WIFI.getName())) == 1 ? true : false;

		bluethoot = cursor.getInt(cursor.getColumnIndex(Profils.Columns.BLUETHOOT.getName())) == 1 ? true : false;

		// TODO 
		// Param not used
		wifiDirect = cursor.getInt(cursor.getColumnIndex(Profils.Columns.WIFI_DIRECT.getName())) == 1 ? true : false;
		nfc = cursor.getInt(cursor.getColumnIndex(Profils.Columns.NFC.getName())) == 1 ? true : false;
		debugMode = cursor.getInt(cursor.getColumnIndex(Profils.Columns.DEBUG_MODE.getName())) == 1 ? true : false;
	}

	public void buildContextMenuListProfil(Context context, IconContextMenu menu) {
		MenuInflater inflater = new MenuInflater(context);
		Menu contextMenu = menu.getMenu();
		inflater.inflate(R.menu.context_menu_list_profil, contextMenu);
		menu.setTitle("Profil : " + name);
	}

	public void applyProfil() {

		//Apply audio mode
		if (mode == ProfilConstant.TYPE_SONNERIE) {
			AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
			audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
			if (ringtonePath != null && ringtoneId != ProfilConstant.ID_DEFAULT_ITEM) {
				Uri ringUri = null;
				if (ringtoneCategory == DataInterface.ProfilConstant.CATEGORY_MUSIC) {
					ringUri = ContentUris.withAppendedId(Media.EXTERNAL_CONTENT_URI, ringtoneId);
				}
				else if (ringtoneCategory == DataInterface.ProfilConstant.CATEGORY_RINGTONE) {
					ringUri = ContentUris.withAppendedId(Media.INTERNAL_CONTENT_URI, ringtoneId);
				}
				try {
					ContentValues values = new ContentValues(2);
					values.put(AudioColumns.IS_RINGTONE, "1");
					values.put(AudioColumns.IS_ALARM, "1");
					mContext.getContentResolver().update(ringUri, values, null, null);
					System.putString(mContext.getContentResolver(), System.RINGTONE, ringUri.toString());
				} catch (UnsupportedOperationException ex) {
					Log.w("[Set ringtone]", ex.getMessage());
				}
			}
			if (notificationPath != null && notificationId != ProfilConstant.ID_DEFAULT_ITEM) {
				Uri notifUri = null;
				if (notificationCategory == DataInterface.ProfilConstant.CATEGORY_MUSIC) {
					notifUri = ContentUris.withAppendedId(Media.EXTERNAL_CONTENT_URI, notificationId);
				}
				else if (notificationCategory == DataInterface.ProfilConstant.CATEGORY_RINGTONE) {
					notifUri = ContentUris.withAppendedId(Media.INTERNAL_CONTENT_URI, notificationId);
				}
				try {
					ContentValues values = new ContentValues(2);
					values.put(AudioColumns.IS_NOTIFICATION, "1");
					values.put(AudioColumns.IS_ALARM, "1");
					mContext.getContentResolver().update(notifUri, values, null, null);
					System.putString(mContext.getContentResolver(), System.NOTIFICATION_SOUND, notifUri.toString());
				} catch (UnsupportedOperationException ex) {
					Log.w("[Set notification]", ex.getMessage());
				}
			}
			if (ringtoneVolume != null) {
				audioManager.setStreamVolume(AudioManager.STREAM_RING, ringtoneVolume, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
			}

			if (soundVolume != null) {
				audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, soundVolume, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
			}

			if (dialPadTouchTones != null) {
				System.putInt(mContext.getContentResolver(), System.DTMF_TONE_WHEN_DIALING, dialPadTouchTones ? 1 : 0);
			}

			if (touchSounds != null) {
				System.putInt(mContext.getContentResolver(), System.SOUND_EFFECTS_ENABLED, touchSounds ? 1 : 0);
			}

			if (screenLockSounds != null) {
				System.putInt(mContext.getContentResolver(), "lockscreen_sounds_enabled", screenLockSounds ? 1 : 0);
			}

			if (vibrateTouch != null) {
				System.putInt(mContext.getContentResolver(), System.HAPTIC_FEEDBACK_ENABLED, vibrateTouch ? 1 : 0);
			}
		}
		else if (mode == ProfilConstant.TYPE_VIBREUR) {
			AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
			audioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
		}
		else if (mode == ProfilConstant.TYPE_SILENCE) {
			AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
			audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
		}

		//Apply brightness mode
		if (ProfilConstant.TYPE_BRIGTNESS_AUTO == brightnessMode) {
			System.putInt(mContext.getContentResolver(), System.SCREEN_BRIGHTNESS_MODE, System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC);
		}
		else if (ProfilConstant.TYPE_BRIGTNESS_MANUAL == brightnessMode) {
			System.putInt(mContext.getContentResolver(), System.SCREEN_BRIGHTNESS_MODE, System.SCREEN_BRIGHTNESS_MODE_MANUAL);
			System.putInt(mContext.getContentResolver(), System.SCREEN_BRIGHTNESS, brightnessIntensity);
		}

		if (sleepTimeScreen != null) {
			System.putInt(mContext.getContentResolver(), System.SCREEN_OFF_TIMEOUT, sleepTimeScreen);
		}

		//Apply wallpaper
		if (backgroundPath != null && backgroundId != ProfilConstant.ID_DEFAULT_ITEM) {
			final WallpaperManager wallManager = WallpaperManager.getInstance(mContext);
			new Thread(new Runnable() {
				@Override
				public void run() {
					Bitmap wallpaper = null;
					try {
						wallpaper = BitmapFactory.decodeFile(backgroundPath);
					} catch (OutOfMemoryError e) {
						Log.w("[Set background OutOfMemoryError]", e.getMessage());
					}
					try {
						wallManager.setBitmap(wallpaper);
					} catch (IOException e) {
						Log.w("[Set background IOException]", e.getMessage());
					} 
				}
			}).start();
		}

		//Apply other parameters
		//Wifi
		WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
		if (wifi != null && wifi == true) {
			wifiManager.setWifiEnabled(true);
		}
		else {
			wifiManager.setWifiEnabled(false);
		}

		//Apply other parameters
		//WifiDirect
		//		WifiP2pManager wdirectManager = (WifiP2pManager) mContext.getSystemService(Context.WIFI_P2P_SERVICE);
		//		if (wifiDirect != null && wifiDirect == true) {
		//
		//		}
		//		else {
		//
		//		}

		//Apply other parameters
		//Bluetooth
		BluetoothAdapter bluetooth = BluetoothAdapter.getDefaultAdapter();
		if (bluethoot != null && bluethoot == true) {
			bluetooth.enable();
		}
		else {
			bluetooth.disable();
		}

		//Apply other parameters
		//NFC
		//		NfcAdapter nfcAdp = NfcAdapter.getDefaultAdapter(mContext);
		//		if (nfc != null && nfc == true) {
		//		}
	}

	public int deleteProfil() {
		return mContext.getContentResolver().delete(Profils.CONTENT_URI, Profils.Columns.ID.getName() + "=?", new String[]{Integer.toString(id)});
	}

	public void editProfil() {
		Intent intent = new Intent(EditProfilActivity.ACTION_EDIT_PROFIL);
		intent.putExtra(EditProfilActivity.EXTRA_ID_PROFIL, id);
		mContext.startActivity(intent);
	}

	public void seeProfil() {
		Intent intent = new Intent(EditProfilActivity.ACTION_SEE_PROFIL);
		intent.putExtra(EditProfilActivity.EXTRA_ID_PROFIL, id);
		mContext.startActivity(intent);
	}

	public void updateProfil(ContentValues values) {
		mContext.getContentResolver().update(Profils.CONTENT_URI, values, Profils.Columns.ID.getName() + "=?", new String[]{Integer.toString(id)});
	}
}

package com.SbDev.AutoProfil.data.Local;

public interface DataInterface {

	public interface PreferenceFields {
		
		public static final String			RADIUS_PROFILE_RESEARCH = "radiusprofilresearch";
		public static final String			TIME_REFRESH_PROFILE_RESEARCH = "timerefreshprofileresearch";
		public static final String			DIST_REFRESH_PROFILE_RESEARCH = "distrefreshprofileresearch";
		public static final String			ASK_CONFIRMATION_SET_PROFILE = "askconfirmationsetprofile";
		public static final String			START_PREFERENCES_IS_INIT = "startpreferencesisinit";
	}
	
	public interface DefaultResearchValue {
		
		public static final int				DEFAULT_RADIUS_RESEARCH_VALUE = 50;
		public static final int				DEFAULT_DIST_REFRESH_PROFILE_RESEARCH = 20;
		public static final int				DEFAULT_TIME_REFRESH_PROFILE_RESEARCH = 60000;
	}
	
	public interface ProfilConstant {
		
		public static final int 			TYPE_SONNERIE = 1;
		public static final int 			TYPE_SILENCE = 2;
		public static final int 			TYPE_VIBREUR = 3;
		public static final int 			TYPE_PLANE = 4;
		
		public static final int				TYPE_BRIGTNESS_AUTO = 5;
		public static final int				TYPE_BRIGTNESS_MANUAL = 6;
		
		public static final int				CATEGORY_MUSIC = 7;
		public static final int				CATEGORY_RINGTONE = 8;
		
		public static final String			ORDER_DESC = "DESC";
		public static final String			ORDER_ASC = "ASC";
		
		public static final int				ID_DEFAULT_ITEM = -1;
		
		public static final int				SLEEP_TIME_15000 = 15000;
		public static final int				SLEEP_TIME_30000 = 30000;
		public static final int				SLEEP_TIME_60000 = 60000;
		public static final int				SLEEP_TIME_120000 = 120000;
		public static final int				SLEEP_TIME_360000 = 360000;
		public static final int				SLEEP_TIME_720000 = 720000;
		public static final int				SLEEP_TIME_2160000 = 2160000;
	}
	
	public interface IdCursorLoader {
		
		public static final int 			ID_CURSOR_RINGTONE = 1;
		public static final int 			ID_CURSOR_NOTIFICATION = 2;
		public static final int 			ID_LOADER_IMAGE_BACKGROUND = 3;
		public static final int				ID_LOADER_PROFIL_LIST = 4;
		public static final int				ID_LOADER_PROFIL_MAP = 5;
	}
}

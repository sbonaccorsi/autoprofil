package com.SbDev.AutoProfil.Application;

import com.SbDev.AutoProfil.R;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;

public class AutoProfilApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		DisplayImageOptions defaultDisplayOptions = new DisplayImageOptions.Builder().cacheInMemory()
				.cacheOnDisc()
				.cacheInMemory()
				.showStubImage(R.drawable.loading_img)
				.showImageForEmptyUri(R.drawable.loading_img)
				.showImageOnFail(R.drawable.loading_img)
				.bitmapConfig(Config.RGB_565)
				.build();
		
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
		.memoryCacheSize(2 * 4096 * 4096)
		.threadPriority(Thread.NORM_PRIORITY - 2)
		.denyCacheImageMultipleSizesInMemory()
		.discCacheFileNameGenerator(new Md5FileNameGenerator())
		.tasksProcessingOrder(QueueProcessingType.LIFO)
		.defaultDisplayImageOptions(defaultDisplayOptions)
		.build();
		ImageLoader.getInstance().init(config);
	}
}

package com.SbDev.AutoProfil.Base;

import android.content.Context;
import android.database.Cursor;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.SbDev.AutoProfil.Map.CustomMarkerFlag;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.SupportMapFragment;

public abstract class BaseMapFragment extends SupportMapFragment implements OnMarkerClickListener, LoaderCallbacks<Cursor>, OnInfoWindowClickListener,
OnMyLocationChangeListener, OnCameraChangeListener, OnMyLocationButtonClickListener {

	protected static int							STROKE_WIDTH_CIRCLE = 5;

	protected static final float					LEVEL_ZOOM_CENTER_USER = 15f; 

	protected GoogleMap								mMap;
	protected Geocoder								mGeocoder;
	protected CustomMarkerFlag						mCustomFlag;
	protected LocationManager						mLocationManager;
	protected Boolean								mCancelCenterUser = false;

	protected abstract void preloadImageOnCustomMarker(Marker marker);

	public BaseMapFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mMap = this.getMap();
		mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
		if (mMap != null) {

			mMap.setMyLocationEnabled(true);
			mMap.getUiSettings().setMyLocationButtonEnabled(true);
			mMap.setOnMarkerClickListener(this);
			mMap.setOnInfoWindowClickListener(this);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mMap != null) {
			mMap.setOnMarkerClickListener(null);
			mMap.setOnInfoWindowClickListener(null);
			mMap.setOnMyLocationChangeListener(null);
		}
	}

	protected void initCursorLoaders(Integer... ids) {

		for (Integer id : ids) {
			getLoaderManager().initLoader(id, null, this);
		}
	}

	protected void centerOnMyLocation() {
		if (mMap != null) {
			Location loc = mMap.getMyLocation();
			if (loc != null) {
				animateCameraWithProp(loc, LEVEL_ZOOM_CENTER_USER, null, null);
				return;
			}
			else {
				mMap.setOnMyLocationChangeListener(this);
			}
		}
	}

	protected void animateCameraKeepProp(Location location) {
		animateCamera(location, mMap.getCameraPosition().zoom,
				mMap.getCameraPosition().bearing, 
				mMap.getCameraPosition().tilt);
	}

	protected void animateCameraKeepProp(LatLng location) {
		animateCamera(this.convertLatLngToLocationObject(location), mMap.getCameraPosition().zoom, 
				mMap.getCameraPosition().bearing, 
				mMap.getCameraPosition().tilt);
	}

	protected void animateCameraWithProp(Location location, Float zoom, Float bearing, Float tilt) {
		animateCamera(location, zoom, bearing, tilt);
	}

	protected void animateCameraWithProp(LatLng location, Float zoom, Float bearing, Float tilt) {
		animateCamera(this.convertLatLngToLocationObject(location), zoom, bearing, tilt);
	}

	protected CircleOptions createCircle(LatLng point, int strokeColor, int fillColor, double radius) {

		CircleOptions circleOption = new CircleOptions()
		.center(point)
		.radius(radius)
		.strokeWidth(STROKE_WIDTH_CIRCLE)
		.strokeColor(getResources().getColor(strokeColor))
		.fillColor(getResources().getColor(fillColor));
		return circleOption;
	}

	private Location convertLatLngToLocationObject(LatLng location) {
		Location loc = new Location(mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) == true ? 
				LocationManager.GPS_PROVIDER : LocationManager.NETWORK_PROVIDER);
		loc.setLatitude(location.latitude);
		loc.setLongitude(location.longitude);
		return loc;
	}

	private void animateCamera(Location location, Float zoom, Float bearing, Float tilt) {
		if (location != null) {
			CameraPosition.Builder builder = new CameraPosition.Builder();
			builder.target(new LatLng(location.getLatitude(), location.getLongitude()));

			if (zoom != null)
				builder.zoom(zoom);
			else
				builder.zoom(mMap.getCameraPosition().zoom);
			if(bearing != null)
				builder.bearing(bearing);
			else
				builder.bearing(mMap.getCameraPosition().bearing);
			if (tilt != null)
				builder.tilt(tilt);
			else
				builder.tilt(mMap.getCameraPosition().tilt);

			mMap.animateCamera(CameraUpdateFactory.newCameraPosition(builder.build()), null);
		}
	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		marker.showInfoWindow();
		return true;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		return null;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor arg1) {
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
	}

	@Override
	public void onInfoWindowClick(Marker marker) {
	}

	@Override
	public void onMyLocationChange(Location location) {
		if (!mCancelCenterUser) {
			mMap.setOnMyLocationChangeListener(null);
			animateCameraWithProp(location, LEVEL_ZOOM_CENTER_USER, null, null);
			mCancelCenterUser = true;
		}
	}

	@Override
	public boolean onMyLocationButtonClick() {

		if (mMap.getMyLocation() != null) {
			if (!mCancelCenterUser) {
				animateCameraWithProp(mMap.getMyLocation(), LEVEL_ZOOM_CENTER_USER, null, null);
				mCancelCenterUser = true;
			}
			else {
				animateCameraKeepProp(mMap.getMyLocation());
			}
		}
		else {
			Toast.makeText(getActivity(), "En attente de localisation", Toast.LENGTH_SHORT).show();
		}

		return true;
	}
}

package com.SbDev.AutoProfil.Base;

import yuku.androidsdk.com.android.internal.view.menu.MenuBuilder;
import yuku.iconcontextmenu.IconContextMenu;
import yuku.iconcontextmenu.IconContextMenu.IconContextItemSelectedListener;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView;
import android.widget.ListView;
import com.actionbarsherlock.app.SherlockListFragment;

public abstract class BaseCursorListFragment extends SherlockListFragment implements LoaderCallbacks<Cursor>, 
OnItemLongClickListener, OnItemClickListener, IconContextItemSelectedListener {

	protected ListView					mList;
	protected CursorAdapter				mAdapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mList = getListView();
		mList.setOnItemLongClickListener(this);
		mList.setOnItemClickListener(this);
	}

	protected void initCursorLoader(Integer... ids) {
		for (int idx = 0; idx < ids.length; ++idx) {
			getLoaderManager().initLoader(ids[idx], null, this);
		}
	}
	
	protected void onCreateContextMenu(IconContextMenu menu, View v, ContextMenuInfo menuInfo) {
		
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		
		final IconContextMenu iconMenu = new IconContextMenu(getActivity(), new MenuBuilder(getActivity().getApplicationContext()));
        iconMenu.setOnIconContextItemSelectedListener(this);
//        iconMenu.setOnDismissListener(this);
//        iconMenu.setOnCancelListener(this);
        onCreateContextMenu(iconMenu, v, menuInfo);
        
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                iconMenu.show();
            }
        });
	}
	
	protected boolean onContextItemSelected(MenuItem item, Object info) {
        return false;
    }
	
	@Override
	public void onIconContextItemSelected(MenuItem item, Object info) {
		onContextItemSelected(item, info);
	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle param) {
		return null;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		mAdapter.changeCursor(cursor);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		mAdapter.changeCursor(null);
	}
	
	@Override
	public void onItemClick(AdapterView<?> adp, View view, int position, long id) {
		onClickItemList(adp, view, position, id);
	}
	
	@Override
	public boolean onItemLongClick(AdapterView<?> adp, View view, int position,
			long id) {
		return onLongClickItemList(adp, view, position, id);
	}
	
	public class CursorAdapter extends SimpleCursorAdapter {

		public CursorAdapter(Context context, int layout, Cursor c,
				String[] from, int[] to, int flags) {
			super(context, layout, c, from, to, flags);
		}

		@Override
		public int getCount() {
			return super.getCount();
		}

		@Override
		public View getView(int position, View view, ViewGroup convertView) {
			View v = super.getView(position, view, convertView);
			Cursor cursor = getCursor();

			initAdapterView(v);
			if (cursor.moveToPosition(position)) {
				configureView(v, cursor);
			}
			return v;
		}
	}
	
	/**
	 * Abstract function call in adapter for initialize view
	 * */
	abstract public void initAdapterView(View v);
	abstract protected void configureView(View view, Cursor cursor);
	
	abstract protected void onClickItemList(AdapterView<?> adp, View view, int position, long id);
	abstract protected boolean onLongClickItemList(AdapterView<?> adp, View view, int postion, long id);
}

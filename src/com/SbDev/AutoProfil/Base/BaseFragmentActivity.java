package com.SbDev.AutoProfil.Base;

import com.SbDev.AutoProfil.R;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import android.os.Bundle;

public abstract class BaseFragmentActivity extends SherlockFragmentActivity {

	abstract protected Integer getContentView();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getContentView());
	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		getSupportMenuInflater().inflate(R.menu.base, menu);
		return super.onCreateOptionsMenu(menu);
	}
}

package com.SbDev.AutoProfil.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.data.Local.MarkerData;
import com.SbDev.AutoProfil.data.Local.Profil;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;

public class CustomMarkerFlag implements InfoWindowAdapter {

	private Context 			mContext;
	private Bitmap				mCustomImg;

	public CustomMarkerFlag(Context context) {
		mContext = context;
	}

	public void setCustomImage(Bitmap loaded) {
		mCustomImg = loaded;
	}

	public void setCustomImage(Drawable drawable) {
		mCustomImg = ((BitmapDrawable)drawable).getBitmap();
	}
	
	@Override
	public View getInfoContents(Marker marker) {
		return this.initializeMarkerFlag(marker);
	}

	@Override
	public View getInfoWindow(Marker marker) {
		return null;
	}

	private View initializeMarkerFlag(final Marker marker) {

		return this.createMarkerView(marker);
	}

	private View createMarkerView(Marker marker) {

		MarkerData data = MarkerData.createFromMarker(mContext, marker);
		View v = LayoutInflater.from(mContext).inflate(R.layout.custom_flag, null);

		if (mCustomImg != null)
			((ImageView)v.findViewById(R.id.imageView_profil_img_marker)).setImageBitmap(mCustomImg);
		((TextView)v.findViewById(R.id.textView_profil_name_marker)).setText(data.name);
		Profil profil = Profil.createFromId(mContext, data.id);
		if (profil.active) {
			((ImageView)v.findViewById(R.id.imageView_profil_active)).setImageDrawable(mContext.getResources().getDrawable(R.drawable.enable));
		}
		else {
			((ImageView)v.findViewById(R.id.imageView_profil_active)).setImageDrawable(mContext.getResources().getDrawable(R.drawable.disable));
		}
		return v;
	}

}

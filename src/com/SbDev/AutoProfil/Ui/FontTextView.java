package com.SbDev.AutoProfil.Ui;

import com.SbDev.AutoProfil.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class FontTextView extends TextView {

	private Typeface		mFont;
	private String 			mFontName;

	public FontTextView() {
		super(null);
	}
	
	public FontTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FontTextView, defStyle, 0);
		mFontName = a.getString(R.styleable.FontTextView_font);

		mFont = Typeface.createFromAsset(getResources().getAssets(), "fonts/" + mFontName);
		if (mFont != null && !isInEditMode()) {
			setTypeface(mFont);
		}
		a.recycle();
	}

	public FontTextView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public FontTextView(Context context) {
		super(context);
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
	}

	@Override
	protected void onFinishInflate() {

		if (mFont != null && !isInEditMode()) {
			setTypeface(mFont);
		}
		super.onFinishInflate();
	}
}

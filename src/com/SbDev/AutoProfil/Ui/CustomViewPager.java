package com.SbDev.AutoProfil.Ui;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomViewPager extends ViewPager {

	private Boolean mScrollEnable;

	public CustomViewPager() {
		super(null);
	}
	
	public CustomViewPager(Context context) {
		super(context);
	}

	public CustomViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public boolean onTouchEvent(MotionEvent arg0) {
		if (mScrollEnable)
			return super.onTouchEvent(arg0);
		return false;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent arg0) {
		if (mScrollEnable)
			return super.onInterceptTouchEvent(arg0);
		return false;
	}

	public void setStateEnable(Boolean value) {
		mScrollEnable = value;
	}
}

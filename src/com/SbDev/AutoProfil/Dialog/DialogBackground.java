package com.SbDev.AutoProfil.Dialog;

import com.SbDev.AutoProfil.R;
import com.actionbarsherlock.app.SherlockFragmentActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class DialogBackground extends AbstractDialogList {

	private static final String ARG_PATH_FILE = "pathFile";
	
	public static void show(SherlockFragmentActivity activity, String title, String pathFile) {
		
		String strings[] = activity.getResources().getStringArray(R.array.dialog_select_background);
		
		DialogBackground dialog = new DialogBackground();
		
		Bundle param = new Bundle();
		param.putString(ARG_TITLE, title);
		param.putStringArray(ARG_ITEMS, strings);
		param.putString(ARG_PATH_FILE, pathFile);
		dialog.setArguments(param);
		dialog.show(activity.getSupportFragmentManager(), TAG);
	}

	@Override
	public Builder buildDialog(Builder builder) {
		mAdapter = new DialogListAdapter();
		((ListView)mCustomView.findViewById(R.id.listView_dialog)).setOnItemClickListener(this);
		((ListView)mCustomView.findViewById(R.id.listView_dialog)).setAdapter(mAdapter);
		return builder;
	}
	
	@Override
	public void onItemClick(AdapterView<?> adp, View view, int position, long id) {

		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.parse("file://" + getArguments().getString(ARG_PATH_FILE)), "image/*");
		startActivity(intent);
		
		dismiss();
	}
	
	@Override
	public void configureView(View convertView, int position) {
		
		ViewHolderBaseDialogList holder = (ViewHolderBaseDialogList) convertView.getTag();
		
		switch (position) {
		case 0:
			holder.mTextCell.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.content_picture), null, null, null);
			break;

		default:
			break;
		}
		holder.mTextCell.setText((String)mAdapter.getItem(position));
	}
}

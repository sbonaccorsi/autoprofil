package com.SbDev.AutoProfil.Dialog;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.SbDev.AutoProfil.R;

import eu.inmite.android.lib.dialogs.SimpleDialogFragment;

public abstract class AbstractDialogList extends SimpleDialogFragment implements OnItemClickListener {

	protected static final String 	ARG_TITLE = "title";
	protected static final String 	ARG_ITEMS = "items";
	protected static final String 	ARG_ID_PROFIL = "idProfil";
	protected static final String 	TAG = "fragmentProfil";
	
	protected View 					mCustomView;
	protected DialogListAdapter		mAdapter;
	
	public abstract void configureView(View convertView, int position);
	public abstract Builder buildDialog(Builder builder);
	
	@Override
	protected Builder build(Builder builder) {
		
		mCustomView = getActivity().getLayoutInflater().inflate(R.layout.dialog_list, null);
		builder.setView(mCustomView);
		builder.setTitle(getTitle());
		return buildDialog(builder);
	}
	
	protected String getTitle() {
		return getArguments().getString(ARG_TITLE);
	}

	protected String[] getItems() {
		return getArguments().getStringArray(ARG_ITEMS);
	}
	
	protected Integer getIdProfil() {
		return getArguments().getInt(ARG_ID_PROFIL);
	}
	
	protected class ViewHolderBaseDialogList {

		public TextView mTextCell;

		public ViewHolderBaseDialogList(View v) {
			mTextCell = (TextView) v.findViewById(R.id.textView_cell_dialog);
		}
	}
	
	protected class DialogListAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return getItems().length;
		}

		@Override
		public Object getItem(int position) {
			return getItems()[position];
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			convertView = initializeView(convertView);
			configureView(convertView, position);
			return convertView;
		}
	}
	
	protected View initializeView(View convertView) {
		
		ViewHolderBaseDialogList holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(getActivity()).inflate(R.layout.cell_base_dialog_list, null);
			holder = new ViewHolderBaseDialogList(convertView);
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolderBaseDialogList) convertView.getTag();
		}
		return convertView;
	}
}

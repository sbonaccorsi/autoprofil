package com.SbDev.AutoProfil.Dialog;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.Profils.Edit.EditProfilActivity;
import com.SbDev.AutoProfil.Service.ManageProfilService;
import com.SbDev.AutoProfil.data.Local.Profil;
import com.SbDev.AutoProfil.data.provider.AutoProfilContent.Profils;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class DialogProfil extends AbstractDialogList {

	public static void show(SherlockFragmentActivity activity, Profil profil) {

		String[] items = activity.getResources().getStringArray(R.array.dialog_profil_list);

		DialogProfil dialog = new DialogProfil();
		Bundle args = new Bundle();
		args.putString(ARG_TITLE, profil.name);
		args.putStringArray(ARG_ITEMS, items);
		args.putInt(ARG_ID_PROFIL, profil.id);
		dialog.setArguments(args);
		dialog.show(activity.getSupportFragmentManager(), TAG);
	}
	
	@Override
	public Builder buildDialog(Builder builder) {
		mAdapter = new DialogListAdapter();
		((ListView)mCustomView.findViewById(R.id.listView_dialog)).setOnItemClickListener(this);
		((ListView)mCustomView.findViewById(R.id.listView_dialog)).setAdapter(mAdapter);
		return builder;
	}
	
	@Override
	public void onItemClick(AdapterView<?> adp, View view, int position, long id) {
		Profil profil = Profil.createFromId(getActivity(), getIdProfil());
		ContentValues values = null;
		switch (position) {
		
		case 0:
			values = new ContentValues();
			values.put(Profils.Columns.ACTIVE.getName(), true);
			profil.updateProfil(values);
			ManageProfilService.startService(getActivity());
			break;
			
		case 1:
			values = new ContentValues();
			values.put(Profils.Columns.ACTIVE.getName(), false);
			values.put(Profils.Columns.IS_SET.getName(), false);
			profil.updateProfil(values);
			break;
			
		case 2:
			profil.seeProfil();
			break;

		case 3:
			profil.editProfil();
			break;

		case 4:
			profil.deleteProfil();
			break;
		}
		
		getDialog().dismiss();
	}
	
	@Override
	public void configureView(View convertView, int position) {
		ViewHolderBaseDialogList holder = (ViewHolderBaseDialogList) convertView.getTag();
		
		switch (position) {
		case 0:
			holder.mTextCell.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.av_play_light), null, null, null);
			break;
		case 1:
			holder.mTextCell.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.av_stop_light), null, null, null);
			break;
		case 2:
			holder.mTextCell.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.action_about_light), null, null, null);
			break;
		case 3:
			holder.mTextCell.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.content_edit_light), null, null, null);
			break;
		case 4:
			holder.mTextCell.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.content_discard_light), null, null, null);
			break;

		default:
			break;
		}
		String text = (String)mAdapter.getItem(position);
		holder.mTextCell.setText(text);
	}
}

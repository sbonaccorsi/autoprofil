package com.SbDev.AutoProfil.Dialog;

import java.util.List;

import com.SbDev.AutoProfil.R;
import com.SbDev.AutoProfil.Profils.Edit.OnAddressSelected;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.bonadevapps.utils.UtilsActivity;
import com.bonadevapps.utils.UtilsGeocoder;

import android.location.Address;
import android.os.Bundle;
import android.util.Log;
import android.util.MonthDisplayHelper;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class DialogSearchAddress extends AbstractDialogList {

	private List<Address> 			mAddressList;
	private OnAddressSelected		mListener;
	
	public static void show(SherlockFragmentActivity activity, String title, List<Address> list, OnAddressSelected listener) {

		String items[] = new String[list.size()];
		for (int idx = 0; idx < list.size(); ++idx) {
			Address address = list.get(idx);
			items[idx] = "";
			items[idx] = UtilsGeocoder.getCompleteAddressName(address);
		}

		DialogSearchAddress dialog = new DialogSearchAddress();
		dialog.setAddressList(list);
		dialog.setOnAddressSelected(listener);
		
		Bundle param = new Bundle();
		param.putString(ARG_TITLE, title);
		param.putStringArray(ARG_ITEMS, items);
		dialog.setArguments(param);
		dialog.show(activity.getSupportFragmentManager(), TAG);
	}

	@Override
	public Builder buildDialog(Builder builder) {
		mAdapter = new DialogListAdapter();
		((ListView)mCustomView.findViewById(R.id.listView_dialog)).setOnItemClickListener(this);
		((ListView)mCustomView.findViewById(R.id.listView_dialog)).setAdapter(mAdapter);
		return builder;
	}

	public void setAddressList(List<Address> list) {
		mAddressList = list;
	}
	
	public void setOnAddressSelected(OnAddressSelected listener) {
		mListener = listener;
	}
	
	@Override
	public void onItemClick(AdapterView<?> adp, View view, int position, long id) {
		mListener.addressSelected(mAddressList.get(position));
		mListener = null;
		dismiss();
	}
	
	@Override
	public void configureView(View convertView, int position) {

		ViewHolderBaseDialogList holder = (ViewHolderBaseDialogList) convertView.getTag();
		holder.mTextCell.setText((String)mAdapter.getItem(position));
	}
}
